﻿-- =============================================
-- Author:		Shaun Salter-Baines
-- Create date: 20.08.2018
-- Description:	Lists out Jobs that have email notifications on them
-- =============================================
CREATE PROCEDURE [sch_job].[EmailSettings] 

AS


--Is Datbase Mail started
EXEC msdb.dbo.sysmail_help_status_sp

-- search the jobs to see which have email configured
SELECT
   -- j.job_id,
    j.name AS [JobName]
 --   js.step_id,
 --   js.command,
 --   j.enabled
FROM    msdb.dbo.sysjobs j
JOIN    msdb.dbo.sysjobsteps js
    ON  js.job_id = j.job_id
WHERE   js.command LIKE '%sp_send_dbmail%' -- replace keyword with the word or stored proc that you are searching for

UNION

SELECT j.[name] AS [JobName]
FROM msdb.[dbo].[sysjobs] j
LEFT JOIN msdb.[dbo].[sysoperators] o 
	ON (j.[notify_email_operator_id] = o.[id])
WHERE j.[enabled] = 1
AND j.[notify_level_email] IN (1, 2, 3)