﻿

--=======================================================================================================
-- Author:		Shaun Salter-Baines
-- Create date: 16.08.2018
-- Description:	Lists out Jobs that have failed
--=======================================================================================================
CREATE PROCEDURE [sch_job].[FailedJobs] as


SET NOCOUNT ON

INSERT INTO [sch_job].[FailedJobDetails]
SELECT 'FAILED' as Status, cast(sj.name as varchar(100)) as "Job Name",
       cast(sjs.step_id as varchar(5)) as "Step ID",
       cast(sjs.step_name as varchar(30)) as "Step Name",
       cast(REPLACE(CONVERT(varchar,convert(datetime,convert(varchar,sjh.run_date)),102),'.','-')+' '+SUBSTRING(RIGHT('000000'+CONVERT(varchar,sjh.run_time),6),1,2)+':'+SUBSTRING(RIGHT('000000'+CONVERT(varchar,sjh.run_time),6),3,2)+':'+SUBSTRING(RIGHT('000000'+CONVERT(varchar,sjh.run_time),6),5,2) as varchar(30)) 'Start Date Time',
       sjh.message as "Message"
FROM msdb.dbo.sysjobs sj
JOIN msdb.dbo.sysjobsteps sjs 
	ON sj.job_id = sjs.job_id
JOIN msdb.dbo.sysjobhistory sjh 
    ON sj.job_id = sjh.job_id and sjs.step_id = sjh.step_id
WHERE sjh.run_status <> 1
  AND cast(sjh.run_date as float)*1000000+sjh.run_time > 
      cast(convert(varchar(8), getdate()-1, 112) as float)*1000000+70000 --yesterday at 7am
UNION
SELECT 'FAILED',cast(sj.name as varchar(100)) as "Job Name",
       'MAIN' as "Step ID",
       'MAIN' as "Step Name",
       cast(REPLACE(CONVERT(varchar,convert(datetime,convert(varchar,sjh.run_date)),102),'.','-')+' '+SUBSTRING(RIGHT('000000'+CONVERT(varchar,sjh.run_time),6),1,2)+':'+SUBSTRING(RIGHT('000000'+CONVERT(varchar,sjh.run_time),6),3,2)+':'+SUBSTRING(RIGHT('000000'+CONVERT(varchar,sjh.run_time),6),5,2) as varchar(30)) 'Start Date Time',
       sjh.message as "Message"
FROM msdb.dbo.sysjobs sj
JOIN msdb.dbo.sysjobhistory sjh 
  ON sj.job_id = sjh.job_id
WHERE sjh.run_status <> 1 and sjh.step_id=0
  AND cast(sjh.run_date as float)*1000000+sjh.run_time >
      cast(convert(varchar(8), getdate()-1, 112) as float)*1000000+70000 --yesterday at

SET ANSI_NULLS ON


/****** Object:  StoredProcedure [sch_log].[Growth]    Script Date: 29/05/2019 14:41:53 ******/
SET ANSI_NULLS ON