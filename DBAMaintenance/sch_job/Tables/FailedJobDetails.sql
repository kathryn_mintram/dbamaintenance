﻿CREATE TABLE [sch_job].[FailedJobDetails] (
    [Status]          VARCHAR (6)     NOT NULL,
    [Job Name]        VARCHAR (100)   NULL,
    [Step ID]         VARCHAR (5)     NULL,
    [Step Name]       VARCHAR (30)    NULL,
    [Start Date Time] VARCHAR (30)    NULL,
    [Message]         NVARCHAR (4000) NULL
);

