﻿



-------------------------------------------------------------------                      
-- sImportCSV                     
-- Purpose: Inserts data from CSV into Temp Table, without having to declare Column Names, this way the SP can be used again and again without hardcoding
--                 
--                      
-- Date		   Who					Version		Comments                      
-- 01.04.2019  Shaun Salter-Baines	1.00		Initial Write                    
-------------------------------------------------------------------    

CREATE PROCEDURE [sch_misc].[ImportCSV]
@FilePath NVARCHAR(MAX)


AS
BEGIN 


DECLARE @ImportData AS NVARCHAR(MAX),
@ImportColumnNames AS NVARCHAR(MAX)


--This Table will hold our Column Names as we are importing dynamically
IF OBJECT_ID('tempdb..#tmpColumns') IS NOT NULL DROP TABLE #tmpColumns
--Drop the Temp Table (although not an actual Temp Table, it's just we want to be able to call this from another proc)
IF OBJECT_ID('DBAMaintenance.sch_security.ADUsers') IS NOT NULL DROP TABLE sch_security.ADUsers


CREATE TABLE #tmpColumns(columnNames VARCHAR(MAX));

SET @ImportColumnNames =
'BULK INSERT #tmpColumns
FROM ' + '''' + @FilePath + '''' +
' WITH (FIRSTROW = 1, LASTROW = 1, FIELDTERMINATOR = '','', ROWTERMINATOR = ''\n'');'

EXEC sp_executesql @ImportColumnNames


SELECT @ImportData = 'CREATE TABLE sch_security.ADUsers (' + REPLACE(columnNames,',',' NVARCHAR(MAX),') + ' NVARCHAR(MAX));
					  BULK INSERT sch_security.ADUsers
				      FROM ' + '''' + @FilePath + '''' + '
				      WITH (FirstRow = 2, FieldTerminator = '','', RowTerminator = ''\n'');'
FROM #tmpColumns

PRINT @ImportData

EXEC sp_executesql @ImportData


END