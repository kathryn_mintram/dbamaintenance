﻿




---- =============================================
---- Author: Shaun Salter-Baines
---- Create date: 01.08.2018
---- Description:	Checks to make sure that Endeca Baseline is complete
---- =============================================

CREATE PROCEDURE [sch_misc].[BaselineCompleteCheck] 

AS

SELECT [DateCompleted]
  FROM [wiggle-v6-01].[dbo].[EndecaBaselineCompleted]