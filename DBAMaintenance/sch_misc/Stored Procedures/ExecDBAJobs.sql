﻿

-- =============================================
-- Author:		Shaun Salter-Baines
-- Create date: 11.12.2018
-- Description:	This SP calls all sGetDBStats_ Procs
-- =============================================

CREATE PROCEDURE [sch_misc].[ExecDBAJobs] 


AS
BEGIN

--We want to make sure that when we collect the DBs we need to scan through, that we don't include this in our final results
SET NOCOUNT ON;


DECLARE @LoopCounter INT , 
		@MaxCounterId INT, 
        @DBName NVARCHAR(100), 
		@SQL NVARCHAR(MAX), 
		@SQL1 NVARCHAR(MAX),
		@ObjectName NVARCHAR(MAX)


IF OBJECT_ID('tempdb..#DBNames') IS NOT NULL DROP TABLE #DBNames


--Store a list of DBs on the Instance so that we can loop through (we want Primary Dbs only)
SELECT
	[Name] AS DBName
	 ,ROW_NUMBER() OVER(ORDER BY [Name] ASC) AS row_no
INTO #DBNames
FROM sys.dm_hadr_availability_replica_states ars
INNER JOIN sys.databases dbs
ON ars.replica_id = dbs.replica_id
WHERE role_desc = 'PRIMARY'
AND name NOT IN ('master', 'tempdb', 'model', 'msdb', 'DBAMaintenance')

--==================================================================================================
--==================================================================================================
SELECT @LoopCounter = min(row_no) , @MaxCounterId = max(row_no) 
FROM #DBNames

WHILE(@LoopCounter IS NOT NULL AND @LoopCounter <= @MaxCounterId)
BEGIN
   SELECT @DBName = DBName
   FROM #DBNames WHERE row_no = @LoopCounter

   EXEC [sch_object].[SPUsage] @DBName
   EXEC [sch_database].[TableSizes] @DBName
   EXEC [sch_database].[TableStats] @DBName
   EXEC [sch_object].[EmptyTables] @DBName

   SET @LoopCounter  = @LoopCounter  + 1        
END


END


/****** Object:  StoredProcedure [sch_object].[EmptyTables]    Script Date: 29/05/2019 14:49:15 ******/
SET ANSI_NULLS ON