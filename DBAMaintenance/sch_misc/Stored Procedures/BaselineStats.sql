﻿




-- =============================================
-- Author:		Shaun Salter-Baines
-- Create date: 11.12.2018
-- Description:	This SP imports a CSV to gather figures for Baseline
-- =============================================

CREATE PROCEDURE [sch_misc].[BaselineStats] 


AS
BEGIN


TRUNCATE TABLE [sch_misc].[BaseLineFiguresStaging]


BULK INSERT [Misc].[BaseLineFiguresStaging]
   FROM 'I:\BaselineFigures\BaseLineLogOutput_Today.csv'
   WITH 
      (
        FIELDTERMINATOR = ',', 
        ROWTERMINATOR = '\n' 
      )

--Now we need to perform some cleaning operations
DELETE
FROM [sch_misc].[BaseLineFiguresStaging]
WHERE BaselineStart = '"BaselineStart"'


--Now insert the figures
INSERT INTO [sch_misc].[BaseLineFigures]
	([BaselineStart]
	,[BaselineEnd]
	,[PriceLoader]
	,[Forge]
	,[Digdx]
	,[Copy]
	,[RestartA]
	,[RestartB]
	,[RestartTotal]
	,[Total])
SELECT
	CAST(REPLACE(STG.BaselineStart, '"', '') AS DATETIME2),
	CAST(REPLACE(STG.BaselineEnd, '"', '') AS DATETIME2),
	CAST(REPLACE(REPLACE(STG.PriceLoader, '"', ''),'-', '') AS TIME),
	CAST(REPLACE(REPLACE(STG.Forge, '"', ''),'-', '') AS TIME),
	CAST(REPLACE(REPLACE(STG.Digdx, '"', ''),'-', '') AS TIME),
	CAST(REPLACE(REPLACE(STG.Copy, '"', ''),'-', '') AS TIME),
	CAST(REPLACE(REPLACE(RestartA, '"', ''),'-', '')AS TIME),
	CAST(REPLACE(REPLACE(STG.RestartB, '"', ''),'-', '') AS TIME),
	CAST(REPLACE(REPLACE(STG.RestartTotal, '"', ''),'-', '')AS TIME),
	CAST(REPLACE(REPLACE(STG.Total, '"', ''),'-', '') AS TIME)
FROM [sch_misc].[BaseLineFiguresStaging] STG
WHERE NOT EXISTS (SELECT * FROM [sch_misc].[BaseLineFigures] WHERE BaselineStart = CAST(REPLACE(STG.BaselineStart, '"', '') AS DATETIME2))


END


/****** Object:  StoredProcedure [sch_misc].[ExecDBAJobs]    Script Date: 29/05/2019 14:48:11 ******/
SET ANSI_NULLS ON