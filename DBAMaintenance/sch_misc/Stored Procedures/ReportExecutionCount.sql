﻿--------------------------------------------------------------------------------------------------------------------
-- [sch_misc].[ReportExecutionCount]
--
-- Date			 Who							Version		Comments
-- 19/11/2019	 Kathryn Mintram				1.00		DBASSRS-3 Initial version
--------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [sch_misc].[ReportExecutionCount]
AS
BEGIN


    DECLARE @RecipientsList NVARCHAR(50),
            @xml NVARCHAR(MAX),
            @body NVARCHAR(MAX),
            @EmailContent NVARCHAR(MAX),
            @RecordsFound INT,
            @queryTime DATETIME2(0) = GETDATE(),
            @pricingRuntime DATETIME2(0);

    SET @RecipientsList = N'DL_SQL_DBA@wiggle.com';

    IF OBJECT_ID('tempdb..#Results') IS NOT NULL
        DROP TABLE #Results;

    CREATE TABLE #Results
         (ReportName NVARCHAR(60),
          Path NVARCHAR(70),
          ExCount INT);
    WITH cte
    AS (SELECT cat.Name,
               Path,
               COUNT(ex.TimeStart) AS ExCount
        FROM
           (SELECT * FROM [ReportServer].[dbo].[Catalog] WHERE type = 2 AND Hidden = 0) AS cat
            LEFT JOIN [ReportServer].[dbo].[ExecutionLog] AS ex
                ON ex.ReportID = cat.ItemID
                   AND ex.TimeStart > DATEADD(YEAR, -1, GETDATE())
        GROUP BY cat.Name,
                 Path)
    INSERT INTO #Results
    SELECT *
    FROM cte
    WHERE ExCount = 0
    ORDER BY ExCount ASC;

    SET @RecordsFound =
      (SELECT COUNT(*) FROM #Results);

    IF @RecordsFound > 0
    BEGIN

        SET @xml = CAST(
                      (   SELECT [ReportName] AS 'td',
                                 '',
                                 [Path] AS 'td',
                                 '',
                                 [ExCount] AS 'td',
                                 ''
                          FROM #Results
                          FOR XML PATH('tr'), ELEMENTS) AS NVARCHAR(MAX));


        SET @body
            = N'<html><body>
	<H2> </H2>
	<p>
	<p>The following reports have not been executed in the last year</p>
	<table border = 1> 
	<tr>
	<th> Report Name </th>
	<th> Path </th>
	<th> Execution Count </th>
	</tr>';

        IF @body IS NOT NULL
        BEGIN
            SET @body = @body + @xml + N'</table></body></html>';
            SET @EmailContent = @body;
        END;


        --=============================================
        -- Send out the email
        --=============================================
        EXEC msdb.dbo.sp_send_dbmail @profile_name = 'SQL Support',
                                     @body = @EmailContent,
                                     @body_format = 'HTML',
                                     @recipients = @RecipientsList,
                                     @subject = 'Unused SSRS Report';
    END;
END