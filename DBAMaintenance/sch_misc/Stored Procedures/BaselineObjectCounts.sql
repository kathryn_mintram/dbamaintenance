﻿-- =============================================================================
-- Author:		Kathryn Mintram
-- Create date: 03/01/2020
-- Description:	Gathers stats on how many objects are being processed each day 
-- for comparison with Baseline trending
-- =============================================================================

CREATE PROCEDURE [sch_misc].[BaselineObjectCounts]
	
AS
BEGIN

				DECLARE @ProductCountTotal INT, 
						@ActiveProductCount INT,
						@BaslineSkusIn INT,
						@BaselineStylesIn INT,
						@PostBaselineRecordCount INT

		SET @ProductCountTotal = (SELECT COUNT(*) FROM [wiggle-v6-01].[dbo].Products)

		SET @ActiveProductCount = (SELECT COUNT(*) AS ActiveProductCount FROM [wiggle-v6-01].[dbo].[ProductDescription] pd
										   JOIN [wiggle-v6-01].[dbo].[products] p
												  ON pd.ProductDescriptionID = p.ProductDescriptionID
									WHERE pd.www = 1 AND (p.www = 1 OR p.product1> 0))

		IF OBJECT_ID('tempdb..#StyleShortDescriptions') IS NOT NULL DROP TABLE #StyleShortDescriptions;
		IF OBJECT_ID('tempdb..#StyleDisplayNames') IS NOT NULL DROP TABLE #StyleDisplayNames;
		IF OBJECT_ID('tempdb..#StyleAssembler') IS NOT NULL DROP TABLE #StyleAssembler;
		IF OBJECT_ID('tempdb..#StyleShipTo') IS NOT NULL DROP TABLE #StyleShipTo;
		IF OBJECT_ID('tempdb..#StyleBullets') IS NOT NULL DROP TABLE #StyleBullets;
		IF OBJECT_ID('tempdb..#SkuAssembler') IS NOT NULL DROP TABLE #SkuAssembler;
		IF OBJECT_ID('tempdb..#SkuStock') IS NOT NULL DROP TABLE #SkuStock;
		IF OBJECT_ID('tempdb..#SkuColourSearch') IS NOT NULL DROP TABLE #SkuColourSearch;
		IF OBJECT_ID('tempdb..#SkuSizeSearch') IS NOT NULL DROP TABLE #SkuSizeSearch;
		IF OBJECT_ID('tempdb..#Skus') IS NOT NULL DROP TABLE #Skus;

		CREATE TABLE #Skus (ProductID NUMERIC(18, 0) NOT NULL PRIMARY KEY CLUSTERED, ProductDescriptionID NUMERIC(18, 0) NOT NULL);

		INSERT #Skus (ProductID, ProductDescriptionID)
		SELECT DISTINCT p.ProductID, p.ProductDescriptionID
		FROM [wiggle-v6-01].dbo.vEndecaProductSkusWiggleOnly epswo
		JOIN [wiggle-v6-01].dbo.Products p ON p.ProductID = CONVERT(NUMERIC(18, 0), epswo.ProductID);

		/* From sEndeca_SkuSizeSearch */
		CREATE TABLE #SkuSizeSearch (ProductID NUMERIC(18, 0) NOT NULL PRIMARY KEY CLUSTERED, au_Size NVARCHAR(120), da_Size NVARCHAR(120), de_Size NVARCHAR(120), en_Size NVARCHAR(120), es_Size NVARCHAR(120), fr_Size NVARCHAR(120), it_Size NVARCHAR(120), ja_Size NVARCHAR(120), nl_Size NVARCHAR(120), nz_Size NVARCHAR(120), ru_Size NVARCHAR(120), sv_Size NVARCHAR(120), us_Size NVARCHAR(120), zh_Size NVARCHAR(120));

		INSERT #SkuSizeSearch (ProductID, au_Size, da_Size, de_Size, en_Size, es_Size, fr_Size, it_Size, ja_Size, nl_Size, nz_Size, ru_Size, sv_Size, us_Size, zh_Size)
		SELECT DISTINCT ProductID, au_Size, da_Size, de_Size, en_Size, es_Size, fr_Size, it_Size, ja_Size, nl_Size, nz_Size, ru_Size, sv_Size, us_Size, zh_Size
		FROM (
			SELECT CONVERT(NVARCHAR(120), l.LanguageCD) + '_Size' as LanguageCD, p.ProductID, COALESCE(ct.DisplayName, en_ct.DisplayName, p.ProductSize) AS ProductSize
			FROM [wiggle-v6-01].dbo.Products p
			JOIN [wiggle-v6-01].dbo.ProductDescription pd ON p.ProductDescriptionID=pd.ProductDescriptionID
			CROSS JOIN [wiggle-v6-01].dbo.languages l
			LEFT JOIN [wiggle-v6-01].dbo.SizeTranslation ct ON p.SizeCD = ct.SizeCD AND ct.LanguageCD = l.LanguageCD
			LEFT JOIN [wiggle-v6-01].dbo.SizeTranslation en_ct ON p.SizeCD = en_ct.SizeCD AND en_ct.LanguageCD = 'en'
			WHERE pd.www = 1 AND REPLACE(COALESCE(ct.DisplayName, p.ProductSize), ' ', '') NOT IN ('','-','0') AND l.IsDisplay = 1) AS s
		PIVOT (
			MIN (ProductSize)
			FOR LanguageCD IN (au_Size, da_Size, de_Size, en_Size, es_Size, fr_Size, it_Size, ja_Size, nl_Size, nz_Size, ru_Size, sv_Size, us_Size, zh_Size)) AS a
		ORDER BY ProductID;

		/* From sEndeca_SkuColourSearch */
		CREATE TABLE #SkuColourSearch (ProductID NUMERIC(18, 0) NOT NULL PRIMARY KEY CLUSTERED, au_Colour NVARCHAR(120), da_Colour NVARCHAR(120), de_Colour NVARCHAR(120), en_Colour NVARCHAR(120), es_Colour NVARCHAR(120), fr_Colour NVARCHAR(120), it_Colour NVARCHAR(120), ja_Colour NVARCHAR(120), nl_Colour NVARCHAR(120), nz_Colour NVARCHAR(120), ru_Colour NVARCHAR(120), sv_Colour NVARCHAR(120), us_Colour NVARCHAR(120), zh_Colour NVARCHAR(120));

		INSERT #SkuColourSearch (ProductID, au_Colour, da_Colour, de_Colour, en_Colour, es_Colour, fr_Colour, it_Colour, ja_Colour, nl_Colour, nz_Colour, ru_Colour, sv_Colour, us_Colour, zh_Colour)
		SELECT DISTINCT ProductID, au_Colour, da_Colour, de_Colour, en_Colour, es_Colour, fr_Colour, it_Colour, ja_Colour, nl_Colour, nz_Colour, ru_Colour, sv_Colour, us_Colour, zh_Colour
		FROM (
			SELECT pd.ProductDescriptionID, CONVERT(NVARCHAR(120), l.LanguageCD)+ '_Colour' AS LanguageCD, p.ProductID, COALESCE(ct.DisplayName, en_ct.ProductColour, p.ProductColour) AS ProductColour
			FROM [wiggle-v6-01].dbo.Products p
			JOIN [wiggle-v6-01].dbo.ProductDescription pd ON p.ProductDescriptionID=pd.ProductDescriptionID
			CROSS JOIN [wiggle-v6-01].dbo.languages l
			LEFT JOIN [wiggle-v6-01].dbo.ColourTranslation ct ON p.ProductColour = ct.ProductColour AND ct.LanguageCD = l.LanguageCD
			LEFT JOIN [wiggle-v6-01].dbo.ColourTranslation en_ct ON p.ProductColour = en_ct.ProductColour AND en_ct.LanguageCD = 'en'
			WHERE pd.www = 1 AND REPLACE(COALESCE(ct.DisplayName, p.ProductColour), ' ', '') NOT IN ('', '-', '0') AND l.IsDisplay = 1 ) AS s
		PIVOT (
			MIN (ProductColour)
			FOR [LanguageCD] IN (au_Colour, da_Colour, de_Colour, en_Colour, es_Colour, fr_Colour, it_Colour, ja_Colour, nl_Colour, nz_Colour, ru_Colour, sv_Colour, us_Colour, zh_Colour)) AS a
		ORDER BY ProductID;

		/* From sEndeca_SkuStock */
		CREATE TABLE #SkuStock (ProductID NUMERIC(18, 0) NOT NULL PRIMARY KEY CLUSTERED, ProductDescriptionID NUMERIC(18, 0) NOT NULL, OnSite BIT NOT NULL, InStock BIT NOT NULL, PreOrderable BIT NOT NULL, Stock INT NOT NULL);

		INSERT #SkuStock (ProductID, ProductDescriptionID, OnSite, InStock, PreOrderable, Stock)
		SELECT p.ProductID,
				p.ProductDescriptionID,
				CASE WHEN (pd.www = 0 OR (pd.WWW = 1 AND CAST(p.www AS INT) + ISNULL(tfs.FreeStock, p.Product1) <= 0)) AND pop.ProductId IS NULL THEN 0 ELSE 1 END AS OnSite,
				CASE WHEN (pd.www = 1 AND ISNULL(tfs.FreeStock, p.Product1) > 0) AND pop.ProductId IS NULL THEN 1 ELSE 0 END AS InStock,
				CASE WHEN pop.ProductId IS NOT NULL THEN 1 ELSE 0 END AS PreOrderable,
				COALESCE(tfs.FreeStock, p.product1, 0) AS Stock
		FROM [wiggle-v6-01].dbo.Products p
		JOIN [wiggle-v6-01].dbo.ProductDescription pd ON p.ProductDescriptionID=pd.ProductDescriptionID
		LEFT JOIN [wiggle-v6-01].dbo.tblFreeStock tfs ON p.ProductID = tfs.ProductID
		LEFT JOIN [wiggle-v6-01].dbo.vEndeca_PreOrderableProducts pop ON p.ProductID = pop.ProductId
		WHERE pd.WWW = 1;

		/* Pull the SKUs together */
		SELECT
			DISTINCT
			s.ProductID, s.ProductDescriptionID,
			sss.au_Size, sss.da_Size, sss.de_Size, sss.en_Size, sss.es_Size, sss.fr_Size, sss.it_Size, sss.ja_Size, sss.nl_Size, sss.nz_Size, sss.ru_Size, sss.sv_Size, sss.us_Size, sss.zh_Size,
			scs.au_Colour, scs.da_Colour, scs.de_Colour, scs.en_Colour, scs.es_Colour, scs.fr_Colour, scs.it_Colour, scs.ja_Colour, scs.nl_Colour, scs.nz_Colour, scs.ru_Colour, scs.sv_Colour, scs.us_Colour, scs.zh_Colour,
			scss.MensClothingSize, scss.WomensClothingSize, scss.ClothingSizeWaist, scss.ClothingSizeBust,
			ss.OnSite, ss.InStock, ss.PreOrderable, ss.Stock,
			sbf.FlavourId
		INTO #SkuAssembler
		FROM
			#Skus s
			LEFT JOIN #SkuSizeSearch sss ON sss.ProductID = s.ProductID
			LEFT JOIN #SkuColourSearch scs ON scs.ProductID = s.ProductID
			LEFT JOIN [wiggle-v6-01].dbo.vEndeca_SkuClothingSizeSearch scss ON scss.ProductID = s.ProductID
			LEFT JOIN #SkuStock ss ON ss.ProductID = s.ProductID
			LEFT JOIN [wiggle-v6-01].dbo.vEndeca_SkuBaseFlavour sbf ON sbf.ProductID = s.ProductID

		/* From sEndeca_GetProductBullets */
		CREATE TABLE #StyleBullets (ProductDescriptionID NUMERIC(18, 0) PRIMARY KEY CLUSTERED, en_ProductBullets NVARCHAR(MAX) NULL, au_ProductBullets NVARCHAR(MAX) NULL, da_ProductBullets NVARCHAR(MAX) NULL, de_ProductBullets NVARCHAR(MAX) NULL, es_ProductBullets NVARCHAR(MAX) NULL, fr_ProductBullets NVARCHAR(MAX) NULL, it_ProductBullets NVARCHAR(MAX) NULL, ja_ProductBullets NVARCHAR(MAX) NULL, nl_ProductBullets NVARCHAR(MAX) NULL, nz_ProductBullets NVARCHAR(MAX) NULL, ru_ProductBullets NVARCHAR(MAX) NULL, sv_ProductBullets NVARCHAR(MAX) NULL, us_ProductBullets NVARCHAR(MAX) NULL, zh_ProductBullets NVARCHAR(MAX) NULL);

		INSERT #StyleBullets (ProductDescriptionID, en_ProductBullets, au_ProductBullets, da_ProductBullets, de_ProductBullets, es_ProductBullets, fr_ProductBullets, it_ProductBullets, ja_ProductBullets, nl_ProductBullets, nz_ProductBullets, ru_ProductBullets, sv_ProductBullets, us_ProductBullets, zh_ProductBullets)
		SELECT ProductDescriptionID, en_ProductBullets, au_ProductBullets, da_ProductBullets, de_ProductBullets, es_ProductBullets, fr_ProductBullets, it_ProductBullets, ja_ProductBullets, nl_ProductBullets, nz_ProductBullets, ru_ProductBullets, sv_ProductBullets, us_ProductBullets, zh_ProductBullets
		FROM (
			SELECT DISTINCT pd.ProductDescriptionID, pd.ProductBullets as ProductBullets, 'en_ProductBullets' as Languages
			FROM [wiggle-v6-01].dbo.ProductDescription pd
			WHERE pd.www = 1
			UNION ALL
			SELECT DISTINCT pdt.ProductDescriptionID, NULLIF(pdt.ProductBullets, ''), pdt.LanguageCD + '_ProductBullets' as Languages
			FROM [wiggle-v6-01].dbo.ProductDescription pd 
			JOIN [wiggle-v6-01].dbo.ProductDescriptionTranslation pdt ON pd.ProductDescriptionId = pdt.ProductDescriptionID
			JOIN [wiggle-v6-01].dbo.Languages l ON pdt.LanguageCD = l.LanguageCD AND l.IsDisplay = 1 AND l.LanguageCD <> 'en'
			WHERE pd.www = 1) Products
		PIVOT (
			MIN(ProductBullets)
			FOR Languages IN (en_ProductBullets, au_ProductBullets, da_ProductBullets, de_ProductBullets, es_ProductBullets, fr_ProductBullets, it_ProductBullets, ja_ProductBullets, nl_ProductBullets, nz_ProductBullets, ru_ProductBullets, sv_ProductBullets, us_ProductBullets, zh_ProductBullets)) pp
		ORDER BY ProductDescriptionID;

		/* From sEndeca_ShipTo */
		CREATE TABLE #StyleShipTo (ProductDescriptionID NUMERIC(18, 0) NOT NULL PRIMARY KEY CLUSTERED, ShipTo_1 BIT NOT NULL, ShipTo_3 BIT NOT NULL, ShipTo_4 BIT NOT NULL, ShipTo_5 BIT NOT NULL, ShipTo_6 BIT NOT NULL, ShipTo_7 BIT NOT NULL, ShipTo_8 BIT NOT NULL, ShipTo_9 BIT NOT NULL, ShipTo_10 BIT NOT NULL, ShipTo_11 BIT NOT NULL, ShipTo_12 BIT NOT NULL, ShipTo_13 BIT NOT NULL, ShipTo_14 BIT NOT NULL, ShipTo_15 BIT NOT NULL, ShipTo_16 BIT NOT NULL, ShipTo_17 BIT NOT NULL, ShipTo_18 BIT NOT NULL, ShipTo_19 BIT NOT NULL, ShipTo_20 BIT NOT NULL, ShipTo_23 BIT NOT NULL, ShipTo_24 BIT NOT NULL, ShipTo_27 BIT NOT NULL, ShipTo_28 BIT NOT NULL, ShipTo_29 BIT NOT NULL, ShipTo_31 BIT NOT NULL, ShipTo_32 BIT NOT NULL, ShipTo_33 BIT NOT NULL, ShipTo_34 BIT NOT NULL, ShipTo_35 BIT NOT NULL, ShipTo_36 BIT NOT NULL, ShipTo_37 BIT NOT NULL, ShipTo_38 BIT NOT NULL, ShipTo_40 BIT NOT NULL, ShipTo_41 BIT NOT NULL, ShipTo_43 BIT NOT NULL, ShipTo_45 BIT NOT NULL, ShipTo_47 BIT NOT NULL, ShipTo_48 BIT NOT NULL, ShipTo_49 BIT NOT NULL, ShipTo_50 BIT NOT NULL, ShipTo_51 BIT NOT NULL, ShipTo_52 BIT NOT NULL, ShipTo_53 BIT NOT NULL, ShipTo_54 BIT NOT NULL, ShipTo_55 BIT NOT NULL, ShipTo_56 BIT NOT NULL, ShipTo_57 BIT NOT NULL, ShipTo_58 BIT NOT NULL, ShipTo_62 BIT NOT NULL, ShipTo_63 BIT NOT NULL, ShipTo_64 BIT NOT NULL, ShipTo_66 BIT NOT NULL, ShipTo_67 BIT NOT NULL, ShipTo_68 BIT NOT NULL, ShipTo_69 BIT NOT NULL, ShipTo_70 BIT NOT NULL, ShipTo_71 BIT NOT NULL, ShipTo_74 BIT NOT NULL, ShipTo_75 BIT NOT NULL, ShipTo_76 BIT NOT NULL, ShipTo_77 BIT NOT NULL, ShipTo_79 BIT NOT NULL, ShipTo_80 BIT NOT NULL, ShipTo_81 BIT NOT NULL, ShipTo_85 BIT NOT NULL, ShipTo_87 BIT NOT NULL, ShipTo_88 BIT NOT NULL, ShipTo_96 BIT NOT NULL, ShipTo_97 BIT NOT NULL, ShipTo_98 BIT NOT NULL, ShipTo_99 BIT NOT NULL, ShipTo_101 BIT NOT NULL, ShipTo_102 BIT NOT NULL, ShipTo_108 BIT NOT NULL);

		INSERT #StyleShipTo (ProductDescriptionID, ShipTo_1, ShipTo_3, ShipTo_4, ShipTo_5, ShipTo_6, ShipTo_7, ShipTo_8, ShipTo_9, ShipTo_10, ShipTo_11, ShipTo_12, ShipTo_13, ShipTo_14, ShipTo_15, ShipTo_16, ShipTo_17, ShipTo_18, ShipTo_19, ShipTo_20, ShipTo_23, ShipTo_24, ShipTo_27, ShipTo_28, ShipTo_29, ShipTo_31, ShipTo_32, ShipTo_33, ShipTo_34, ShipTo_35, ShipTo_36, ShipTo_37, ShipTo_38, ShipTo_40, ShipTo_41, ShipTo_43, ShipTo_45, ShipTo_47, ShipTo_48, ShipTo_49, ShipTo_50, ShipTo_51, ShipTo_52, ShipTo_53, ShipTo_54, ShipTo_55, ShipTo_56, ShipTo_57, ShipTo_58, ShipTo_62, ShipTo_63, ShipTo_64, ShipTo_66, ShipTo_67, ShipTo_68, ShipTo_69, ShipTo_70, ShipTo_71, ShipTo_74, ShipTo_75, ShipTo_76, ShipTo_77, ShipTo_79, ShipTo_80, ShipTo_81, ShipTo_85, ShipTo_87, ShipTo_88, ShipTo_96, ShipTo_97, ShipTo_98, ShipTo_99, ShipTo_101, ShipTo_102, ShipTo_108)
		SELECT ProductDescriptionID, ISNULL(ShipTo_1, 0), ISNULL(ShipTo_3, 0), ISNULL(ShipTo_4, 0), ISNULL(ShipTo_5, 0), ISNULL(ShipTo_6, 0), ISNULL(ShipTo_7, 0), ISNULL(ShipTo_8, 0), ISNULL(ShipTo_9, 0), ISNULL(ShipTo_10, 0), ISNULL(ShipTo_11, 0), ISNULL(ShipTo_12, 0), ISNULL(ShipTo_13, 0), ISNULL(ShipTo_14, 0), ISNULL(ShipTo_15, 0), ISNULL(ShipTo_16, 0), ISNULL(ShipTo_17, 0), ISNULL(ShipTo_18, 0), ISNULL(ShipTo_19, 0), ISNULL(ShipTo_20, 0), ISNULL(ShipTo_23, 0), ISNULL(ShipTo_24, 0), ISNULL(ShipTo_27, 0), ISNULL(ShipTo_28, 0), ISNULL(ShipTo_29, 0), ISNULL(ShipTo_31, 0), ISNULL(ShipTo_32, 0), ISNULL(ShipTo_33, 0), ISNULL(ShipTo_34, 0), ISNULL(ShipTo_35, 0), ISNULL(ShipTo_36, 0), ISNULL(ShipTo_37, 0), ISNULL(ShipTo_38, 0), ISNULL(ShipTo_40, 0), ISNULL(ShipTo_41, 0), ISNULL(ShipTo_43, 0), ISNULL(ShipTo_45, 0), ISNULL(ShipTo_47, 0), ISNULL(ShipTo_48, 0), ISNULL(ShipTo_49, 0), ISNULL(ShipTo_50, 0), ISNULL(ShipTo_51, 0), ISNULL(ShipTo_52, 0), ISNULL(ShipTo_53, 0), ISNULL(ShipTo_54, 0), ISNULL(ShipTo_55, 0), ISNULL(ShipTo_56, 0), ISNULL(ShipTo_57, 0), ISNULL(ShipTo_58, 0), ISNULL(ShipTo_62, 0), ISNULL(ShipTo_63, 0), ISNULL(ShipTo_64, 0), ISNULL(ShipTo_66, 0), ISNULL(ShipTo_67, 0), ISNULL(ShipTo_68, 0), ISNULL(ShipTo_69, 0), ISNULL(ShipTo_70, 0), ISNULL(ShipTo_71, 0), ISNULL(ShipTo_74, 0), ISNULL(ShipTo_75, 0), ISNULL(ShipTo_76, 0), ISNULL(ShipTo_77, 0), ISNULL(ShipTo_79, 0), ISNULL(ShipTo_80, 0), ISNULL(ShipTo_81, 0), ISNULL(ShipTo_85, 0), ISNULL(ShipTo_87, 0), ISNULL(ShipTo_88, 0), ISNULL(ShipTo_96, 0), ISNULL(ShipTo_97, 0), ISNULL(ShipTo_98, 0), ISNULL(ShipTo_99, 0), ISNULL(ShipTo_101, 0), ISNULL(ShipTo_102, 0), ISNULL(ShipTo_108, 0)
		FROM
			(SELECT ProductDescriptionID, 'ShipTo_' + CONVERT(VARCHAR(3), CountryID) AS ShipToCountry, 1 as Ships 
			FROM [wiggle-v6-01].dbo.CountryProductShipping) a
		PIVOT (
			MAX(Ships)
			FOR ShipToCountry IN (ShipTo_1, ShipTo_3, ShipTo_4, ShipTo_5, ShipTo_6, ShipTo_7, ShipTo_8, ShipTo_9, ShipTo_10, ShipTo_11, ShipTo_12, ShipTo_13, ShipTo_14, ShipTo_15, ShipTo_16, ShipTo_17, ShipTo_18, ShipTo_19, ShipTo_20, ShipTo_23, ShipTo_24, ShipTo_27, ShipTo_28, ShipTo_29, ShipTo_31, ShipTo_32, ShipTo_33, ShipTo_34, ShipTo_35, ShipTo_36, ShipTo_37, ShipTo_38, ShipTo_40, ShipTo_41, ShipTo_43, ShipTo_45, ShipTo_47, ShipTo_48, ShipTo_49, ShipTo_50, ShipTo_51, ShipTo_52, ShipTo_53, ShipTo_54, ShipTo_55, ShipTo_56, ShipTo_57, ShipTo_58, ShipTo_62, ShipTo_63, ShipTo_64, ShipTo_66, ShipTo_67, ShipTo_68, ShipTo_69, ShipTo_70, ShipTo_71, ShipTo_74, ShipTo_75, ShipTo_76, ShipTo_77, ShipTo_79, ShipTo_80, ShipTo_81, ShipTo_85, ShipTo_87, ShipTo_88, ShipTo_96, ShipTo_97, ShipTo_98, ShipTo_99, ShipTo_101, ShipTo_102, ShipTo_108)) pp
		ORDER BY ProductDescriptionID;

		/* From sEndeca_GetDisplayNames */
		CREATE TABLE #StyleDisplayNames (ProductDescriptionID NUMERIC(18, 0) NOT NULL PRIMARY KEY CLUSTERED, en_DisplayName NVARCHAR(100) NOT NULL, au_DisplayName NVARCHAR(100) NOT NULL, da_DisplayName NVARCHAR(100) NOT NULL, de_DisplayName NVARCHAR(100) NOT NULL, es_DisplayName NVARCHAR(100) NOT NULL, fr_DisplayName NVARCHAR(100) NOT NULL, it_DisplayName NVARCHAR(100) NOT NULL, ja_DisplayName NVARCHAR(100) NOT NULL, nl_DisplayName NVARCHAR(100) NOT NULL, nz_DisplayName NVARCHAR(100) NOT NULL, ru_DisplayName NVARCHAR(100) NOT NULL, sv_DisplayName NVARCHAR(100) NOT NULL, us_DisplayName NVARCHAR(100) NOT NULL, zh_DisplayName NVARCHAR(100) NOT NULL);

		INSERT #StyleDisplayNames (ProductDescriptionID, en_DisplayName, au_DisplayName, da_DisplayName, de_DisplayName, es_DisplayName, fr_DisplayName, it_DisplayName, ja_DisplayName, nl_DisplayName, nz_DisplayName, ru_DisplayName, sv_DisplayName, us_DisplayName, zh_DisplayName)
		SELECT ProductDescriptionID, en_DisplayName, ISNULL(NULLIF(au_DisplayName, ''), en_DisplayName), ISNULL(NULLIF(da_DisplayName, ''), en_DisplayName), ISNULL(NULLIF(de_DisplayName, ''), en_DisplayName), ISNULL(NULLIF(es_DisplayName, ''), en_DisplayName), ISNULL(NULLIF(fr_DisplayName, ''), en_DisplayName), ISNULL(NULLIF(it_DisplayName, ''), en_DisplayName), ISNULL(NULLIF(ja_DisplayName, ''), en_DisplayName), ISNULL(NULLIF(nl_DisplayName, ''), en_DisplayName), ISNULL(NULLIF(nz_DisplayName, ''), en_DisplayName), ISNULL(NULLIF(ru_DisplayName, ''), en_DisplayName), ISNULL(NULLIF(sv_DisplayName, ''), en_DisplayName), ISNULL(NULLIF(us_DisplayName, ''), en_DisplayName), ISNULL(NULLIF(zh_DisplayName, ''), en_DisplayName)
		FROM
			(SELECT DISTINCT pd.ProductDescriptionID, ISNULL(pd.Manufacturer + ' ', '') + pd.ProductName AS ProductName, 'en_displayname' as Languages
			FROM [wiggle-v6-01].dbo.ProductDescription pd
			WHERE pd.www = 1
			UNION ALL
			SELECT DISTINCT pdt.ProductDescriptionID, pdt.ProductName, pdt.LanguageCD + '_DisplayName' AS Languages
			FROM [wiggle-v6-01].dbo.ProductDescription pd
			JOIN [wiggle-v6-01].dbo.ProductDescriptionTranslation pdt ON pd.ProductDescriptionId = pdt.ProductDescriptionID
			JOIN [wiggle-v6-01].dbo.Languages l ON pdt.LanguageCD = l.LanguageCD AND l.IsDisplay = 1 AND l.LanguageCD <> 'en'
			WHERE pd.www = 1) Products
		PIVOT (
			MIN(ProductName)
			FOR Languages IN (en_DisplayName, au_DisplayName, da_DisplayName, de_DisplayName, es_DisplayName, fr_DisplayName, it_DisplayName, ja_DisplayName, nl_DisplayName, nz_DisplayName, ru_DisplayName, sv_DisplayName, us_DisplayName, zh_DisplayName)) pp
		ORDER BY ProductDescriptionID;

		/* From sEndeca_GetShortDescriptions */
		CREATE TABLE #StyleShortDescriptions (ProductDescriptionID NUMERIC(18, 0) NOT NULL PRIMARY KEY CLUSTERED, en_ShortDescription NVARCHAR(MAX) NULL, au_ShortDescription NVARCHAR(MAX) NULL, da_ShortDescription NVARCHAR(MAX) NULL, de_ShortDescription NVARCHAR(MAX) NULL, es_ShortDescription NVARCHAR(MAX) NULL, fr_ShortDescription NVARCHAR(MAX) NULL, it_ShortDescription NVARCHAR(MAX) NULL, ja_ShortDescription NVARCHAR(MAX) NULL, nl_ShortDescription NVARCHAR(MAX) NULL, nz_ShortDescription NVARCHAR(MAX) NULL, ru_ShortDescription NVARCHAR(MAX) NULL, sv_ShortDescription NVARCHAR(MAX) NULL, us_ShortDescription NVARCHAR(MAX) NULL, zh_ShortDescription NVARCHAR(MAX) NULL);

		INSERT #StyleShortDescriptions (ProductDescriptionID, en_ShortDescription, au_ShortDescription, da_ShortDescription, de_ShortDescription, es_ShortDescription, fr_ShortDescription, it_ShortDescription, ja_ShortDescription, nl_ShortDescription, nz_ShortDescription, ru_ShortDescription, sv_ShortDescription, us_ShortDescription, zh_ShortDescription)
		SELECT ProductDescriptionID, NULLIF(en_ShortDescription, N'') en_ShortDescription, NULLIF(au_ShortDescription, N'') au_ShortDescription, NULLIF(da_ShortDescription, N'') da_ShortDescription, NULLIF(de_ShortDescription, N'') de_ShortDescription, NULLIF(es_ShortDescription, N'') es_ShortDescription, NULLIF(fr_ShortDescription, N'') fr_ShortDescription, NULLIF(it_ShortDescription, N'') it_ShortDescription, NULLIF(ja_ShortDescription, N'') ja_ShortDescription, NULLIF(nl_ShortDescription, N'') nl_ShortDescription, NULLIF(nz_ShortDescription, N'') nz_ShortDescription, NULLIF(ru_ShortDescription, N'') ru_ShortDescription, NULLIF(sv_ShortDescription, N'') sv_ShortDescription, NULLIF(us_ShortDescription, N'') us_ShortDescription, NULLIF(zh_ShortDescription, N'') zh_ShortDescription
		FROM
			(SELECT DISTINCT pd.ProductDescriptionID, CONVERT(NVARCHAR(MAX), pd.ProductShortDescription) as ShortDescription, 'en_ShortDescription' AS Languages
			FROM [wiggle-v6-01].dbo.ProductDescription pd 
			WHERE pd.www = 1
			UNION ALL
			SELECT DISTINCT pdt.ProductDescriptionID, pdt.ProductShortDescription, pdt.LanguageCD + '_ShortDescription' AS Languages
			FROM [wiggle-v6-01].dbo.ProductDescription pd
			JOIN [wiggle-v6-01].dbo.ProductDescriptionTranslation pdt ON pd.ProductDescriptionId = pdt.ProductDescriptionID
			JOIN [wiggle-v6-01].dbo.Languages l ON pdt.LanguageCD = l.LanguageCD AND l.IsDisplay = 1 AND l.LanguageCD <> 'en'
			WHERE pd.www = 1) Products
		PIVOT (
			MIN(ShortDescription)
			FOR Languages IN (en_ShortDescription, au_ShortDescription, da_ShortDescription, de_ShortDescription, es_ShortDescription, fr_ShortDescription, it_ShortDescription, ja_ShortDescription, nl_ShortDescription, nz_ShortDescription, ru_ShortDescription, sv_ShortDescription, us_ShortDescription, zh_ShortDescription)) pp
		ORDER BY ProductDescriptionID;

		SET @BaslineSkusIn = (SELECT COUNT(*) FROM #SkuAssembler);

		SET @BaselineStylesIn = (SELECT COUNT(*)
		FROM
			(SELECT DISTINCT ProductDescriptionID FROM #SkuAssembler) s
			LEFT JOIN [wiggle-v6-01].dbo.vEndeca_ProductCoreData pcd ON pcd.ProductDescriptionID = s.ProductDescriptionID
			LEFT JOIN [wiggle-v6-01].dbo.vEndeca_ProductCategories pc ON pc.ProductDescriptionID = s.ProductDescriptionID
			LEFT JOIN #StyleShipTo sst ON sst.ProductDescriptionID = s.ProductDescriptionID
			LEFT JOIN [wiggle-v6-01].dbo.vEndeca_ProductMainImages pmi ON pmi.ProductDescriptionID = s.ProductDescriptionID
			LEFT JOIN #StyleDisplayNames dn ON dn.ProductDescriptionID = s.ProductDescriptionID
			LEFT JOIN [wiggle-v6-01].dbo.ProductDescriptionV12Options v12 ON v12.ProductDescriptionID = s.ProductDescriptionID
			LEFT JOIN #StyleShortDescriptions sd ON sd.ProductDescriptionID = s.ProductDescriptionID
			LEFT JOIN #StyleBullets sb ON sb.ProductDescriptionID = s.ProductDescriptionID
			LEFT JOIN WiggleWeb.dbo.vEndeca_ProductReviewData prd ON prd.ProductDescriptionID = s.ProductDescriptionID);

		IF OBJECT_ID('tempdb..#StyleShortDescriptions') IS NOT NULL DROP TABLE #StyleShortDescriptions;
		IF OBJECT_ID('tempdb..#StyleDisplayNames') IS NOT NULL DROP TABLE #StyleDisplayNames;
		IF OBJECT_ID('tempdb..#StyleAssembler') IS NOT NULL DROP TABLE #StyleAssembler;
		IF OBJECT_ID('tempdb..#StyleShipTo') IS NOT NULL DROP TABLE #StyleShipTo;
		IF OBJECT_ID('tempdb..#StyleBullets') IS NOT NULL DROP TABLE #StyleBullets;
		IF OBJECT_ID('tempdb..#SkuAssembler') IS NOT NULL DROP TABLE #SkuAssembler;
		IF OBJECT_ID('tempdb..#SkuStock') IS NOT NULL DROP TABLE #SkuStock;
		IF OBJECT_ID('tempdb..#SkuColourSearch') IS NOT NULL DROP TABLE #SkuColourSearch;
		IF OBJECT_ID('tempdb..#SkuSizeSearch') IS NOT NULL DROP TABLE #SkuSizeSearch;
		IF OBJECT_ID('tempdb..#Skus') IS NOT NULL DROP TABLE #Skus;

		INSERT INTO [sch_misc].[BaselineObjects]
           ([ProductCountTotal]
           ,[ActiveProductCount]
           ,[BaselineSkusIn]
           ,[BaselineStylesIn]
           ,[PostBaselineRecordCount])
     VALUES
           (@ProductCountTotal
           ,@ActiveProductCount
		   ,@BaslineSkusIn
		   ,@BaselineStylesIn
           ,NULL)

END