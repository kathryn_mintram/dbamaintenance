﻿CREATE TABLE [sch_misc].[BaselineObjects] (
    [ID]						INT IDENTITY (1, 1) NOT NULL,
	[ProductCountTotal]			INT	NULL,
	[ActiveProductCount]		INT	NULL,
	[BaselineSkusIn]			INT NULL,
	[BaselineStylesIn]			INT NULL,
	[PostBaselineRecordCount]	INT NULL,
    [DateLogged]				DATETIME2 (0) DEFAULT (getdate()) NOT NULL
);







