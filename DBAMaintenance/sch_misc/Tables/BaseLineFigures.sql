﻿CREATE TABLE [sch_misc].[BaseLineFigures] (
    [ID]            INT           IDENTITY (1, 1) NOT NULL,
    [BaselineStart] DATETIME2 (7) NOT NULL,
    [BaselineEnd]   DATETIME2 (7) NULL,
    [PriceLoader]   TIME (7)      NULL,
    [Forge]         TIME (7)      NULL,
    [Digdx]         TIME (7)      NULL,
    [Copy]          TIME (7)      NULL,
    [RestartA]      TIME (7)      NULL,
    [RestartB]      TIME (7)      NULL,
    [RestartTotal]  TIME (7)      NULL,
    [Total]         TIME (7)      NULL,
    CONSTRAINT [PK_BaselineFigures] PRIMARY KEY CLUSTERED ([ID] ASC, [BaselineStart] ASC)
);



