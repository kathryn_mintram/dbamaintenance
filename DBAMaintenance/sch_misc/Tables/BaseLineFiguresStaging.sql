﻿CREATE TABLE [sch_misc].[BaseLineFiguresStaging] (
    [BaselineStart] VARCHAR (100) NULL,
    [BaselineEnd]   VARCHAR (100) NULL,
    [PriceLoader]   VARCHAR (100) NULL,
    [Forge]         VARCHAR (100) NULL,
    [Digdx]         VARCHAR (100) NULL,
    [Copy]          VARCHAR (100) NULL,
    [RestartA]      VARCHAR (100) NULL,
    [RestartB]      VARCHAR (100) NULL,
    [RestartTotal]  VARCHAR (100) NULL,
    [Total]         VARCHAR (100) NULL
);

