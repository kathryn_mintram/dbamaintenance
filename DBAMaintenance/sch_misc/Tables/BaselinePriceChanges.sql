﻿CREATE TABLE [sch_misc].[BaselinePriceChanges]
(
	[TargetDate] DATETIME2(0) NOT NULL, 
	[GenerationDate] DATETIME2 NOT NULL,
    [Location] INT NOT NULL, 
    [ChangeCount] INT NOT NULL 
)
