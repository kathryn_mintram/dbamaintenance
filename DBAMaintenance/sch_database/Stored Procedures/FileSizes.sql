﻿
-- =============================================
-- Author:		Kathryn Mintram
-- Create date: 07/12/2018
-- Description:	Retrieves information regarding database file sizes
-- https://blog.sqlauthority.com/2010/02/08/sql-server-find-the-size-of-database-file-find-the-size-of-log-file/
-- =============================================
CREATE PROCEDURE [sch_database].[FileSizes] 
AS
	BEGIN
		SET NOCOUNT ON 
		SET NOCOUNT ON;

		INSERT INTO [sch_database].[FileSizeDetails] 
			(DatabaseName, LogicalName, PhysicalName, SizeMB) 
		SELECT DB_NAME(database_id) AS DatabaseName
				,Name AS Logical_Name
				,Physical_Name
				,(size*8)/1024 SizeMB
		FROM sys.master_files
	END


/****** Object:  StoredProcedure [sch_database].[FileSpaceStats]    Script Date: 29/05/2019 14:39:29 ******/
SET ANSI_NULLS ON