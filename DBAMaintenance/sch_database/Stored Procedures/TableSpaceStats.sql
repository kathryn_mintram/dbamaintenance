﻿CREATE PROCEDURE [sch_database].[TableSpaceStats] (@RunLocal BIT = 0)
AS
BEGIN 
 
DECLARE @dbName sysname 
 
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE name = 'TableSpaceDetails' AND type='U') 
BEGIN 
	CREATE TABLE [sch_database].[TableSpaceDetails]
	( 
		Server_Name sysname NOT NULL, 
		dbName sysname NOT NULL, 
		TableName sysname NULL, 
		SchemaName sysname NULL, 
		RowCounts BIGINT NULL, 
		TotalSpaceKB decimal(20, 1) NULL, 
		TotalSpaceMB decimal(20, 1) NULL, 
		UsedSpaceKB decimal(20, 1) NULL, 
		UsedSpaceMB decimal(20, 1) NULL, 
		UnusedSpaceKB decimal(20, 1) NULL, 
		UnusedSpaceMB decimal(20, 1) NULL, 
		Report_Date datetime default getdate() 
	)
	--ON PRIMARY 
END 

IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE name LIKE '#TableSpaceStats%') 
BEGIN
	DROP TABLE #TableSpaceStats 
END
 
CREATE TABLE #TableSpaceStats
	(	
		RowID int IDENTITY PRIMARY KEY, 
		Server_Name sysname NOT NULL, 
		dbName sysname NOT NULL, 
		TableName sysname NULL, 
		SchemaName sysname NULL, 
		RowCounts BIGINT NULL, 
		TotalSpaceKB decimal(20, 1) NULL, 
		TotalSpaceMB decimal(20, 1) NULL, 
		UsedSpaceKB decimal(20, 1) NULL, 
		UsedSpaceMB decimal(20, 1) NULL, 
		UnusedSpaceKB decimal(20, 1) NULL, 
		UnusedSpaceMB decimal(20, 1) NULL, 
		Report_Date datetime default getdate() 
	)

DECLARE @string VARCHAR(8000)  
DECLARE cur_dbName CURSOR FOR 
 
SELECT name 
FROM master..sysdatabases
 
OPEN cur_dbName 
 
FETCH NEXT FROM cur_dbName into @dbName 
WHILE @@FETCH_Status=0 
BEGIN 

SET @string = 'USE [' + @dbName + '] SELECT @@servername AS Server_Name, DB_NAME() AS dbName,
	t.NAME AS TableName,
    s.Name AS SchemaName,
    p.rows AS RowCounts,
    SUM(a.total_pages) * 8 AS TotalSpaceKB, 
    CAST(ROUND(((SUM(a.total_pages) * 8) / 1024.00), 2) AS NUMERIC(36, 2)) AS TotalSpaceMB,
    SUM(a.used_pages) * 8 AS UsedSpaceKB, 
    CAST(ROUND(((SUM(a.used_pages) * 8) / 1024.00), 2) AS NUMERIC(36, 2)) AS UsedSpaceMB, 
    (SUM(a.total_pages) - SUM(a.used_pages)) * 8 AS UnusedSpaceKB,
    CAST(ROUND(((SUM(a.total_pages) - SUM(a.used_pages)) * 8) / 1024.00, 2) AS NUMERIC(36, 2)) AS UnusedSpaceMB
FROM 
    sys.tables t
INNER JOIN      
    sys.indexes i ON t.OBJECT_ID = i.object_id
INNER JOIN 
    sys.partitions p ON i.object_id = p.OBJECT_ID AND i.index_id = p.index_id
INNER JOIN 
    sys.allocation_units a ON p.partition_id = a.container_id
LEFT OUTER JOIN 
    sys.schemas s ON t.schema_id = s.schema_id
WHERE t.NAME NOT LIKE ''dt%''
    AND t.is_ms_shipped = 0
    AND i.OBJECT_ID > 255 
GROUP BY 
    t.Name, s.Name, p.Rows
ORDER BY 
    t.Name' 
INSERT INTO #TableSpaceStats (Server_Name, dbName, TableName, SchemaName, RowCounts, TotalSpaceKB,TotalSpaceMB,UsedSpaceKB,UsedSpaceMB,UnusedSpaceKB,UnusedSpaceMB) 
EXEC (@string) 

FETCH NEXT FROM cur_dbName INTO @dbName 
END 
CLOSE cur_dbName 
DEALLOCATE cur_dbName 

INSERT [sch_database].[TableSpaceDetails]
	(Server_Name, dbName, TableName, SchemaName, RowCounts, TotalSpaceKB,TotalSpaceMB,UsedSpaceKB,UsedSpaceMB,UnusedSpaceKB,UnusedSpaceMB) 
SELECT Server_Name, dbName, TableName, SchemaName, RowCounts, TotalSpaceKB,TotalSpaceMB,UsedSpaceKB,UsedSpaceMB,UnusedSpaceKB,UnusedSpaceMB
FROM #TableSpaceStats

DROP TABLE #TableSpaceStats


END


/****** Object:  StoredProcedure [sch_database].[TableStats]    Script Date: 29/05/2019 14:40:55 ******/
SET ANSI_NULLS ON