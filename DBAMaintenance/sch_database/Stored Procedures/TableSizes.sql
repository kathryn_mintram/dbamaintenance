﻿

--=======================================================================================================

-- Name/Description: Get Table Information for Each DataBase
-- Author: Shaun Salter-Baines - inspired by the script on mssqltips.com
-- Date: 02.08.2018
--=======================================================================================================

CREATE PROCEDURE [sch_database].[TableSizes]
@DBName VARCHAR(100)


AS


CREATE TABLE #TableSizes
(
TableName VARCHAR(150),
SchemaName VARCHAR(150),
RowCounts INT,
TotalSpaceKB NUMERIC(36,2),
TotalSpaceMB NUMERIC(36,2),
UsedSpaceKB NUMERIC(36,2),
UsedSpaceMB NUMERIC(36,2),
UnusedSpaceKB NUMERIC(36,2),
UnusedSpaceMB NUMERIC(36,2)
)


DECLARE @SQL NVARCHAR(MAX)

SET @SQL = '
USE ' + QUOTENAME(@DBName) +
'
INSERT INTO #TableSizes (TableName, SchemaName, RowCounts, TotalSpaceKB, TotalSpaceMB, UsedSpaceKB, UsedSpaceMB, UnusedSpaceKB, UnusedSpaceMB)
SELECT 
TOP 20
    t.NAME AS TableName,
    s.Name AS SchemaName,
    p.rows AS RowCounts,
    SUM(a.total_pages) * 8 AS TotalSpaceKB, 
    CAST(ROUND(((SUM(a.total_pages) * 8) / 1024.00), 2) AS NUMERIC(36, 2)) AS TotalSpaceMB,
    SUM(a.used_pages) * 8 AS UsedSpaceKB, 
    CAST(ROUND(((SUM(a.used_pages) * 8) / 1024.00), 2) AS NUMERIC(36, 2)) AS UsedSpaceMB, 
    (SUM(a.total_pages) - SUM(a.used_pages)) * 8 AS UnusedSpaceKB,
    CAST(ROUND(((SUM(a.total_pages) - SUM(a.used_pages)) * 8) / 1024.00, 2) AS NUMERIC(36, 2)) AS UnusedSpaceMB
FROM 
    sys.tables t
INNER JOIN      
    sys.indexes i ON t.OBJECT_ID = i.object_id
INNER JOIN 
    sys.partitions p ON i.object_id = p.OBJECT_ID AND i.index_id = p.index_id
INNER JOIN 
    sys.allocation_units a ON p.partition_id = a.container_id
LEFT OUTER JOIN 
    sys.schemas s ON t.schema_id = s.schema_id
WHERE 
    t.NAME NOT LIKE ''dt%'' 
    AND t.is_ms_shipped = 0
    AND i.OBJECT_ID > 255 
GROUP BY 
    t.Name, s.Name, p.Rows
ORDER BY 
    t.Name'

EXEC sp_executesql @SQL

INSERT INTO [DBAMaintenance].[sch_database].[TableSizeDetails] (DatabaseName,TableName,RowCounts,SpaceInGB,UnusedSpaceMB,DateCaptured)
SELECT
QUOTENAME(@DBName) AS DBName
,TableName
,REPLACE(CONVERT(VARCHAR,CONVERT(MONEY,RowCounts),1), '.00','') AS NoOfRows
,TotalSpaceMB/1024 AS SpaceInGB
,UnusedSpaceMB
,GETDATE()
FROM #TableSizes ORDER BY TotalSpaceMB DESC


/****** Object:  StoredProcedure [sch_database].[TableSpaceStats]    Script Date: 29/05/2019 14:40:29 ******/
SET ANSI_NULLS ON