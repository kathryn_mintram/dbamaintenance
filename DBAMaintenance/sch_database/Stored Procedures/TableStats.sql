﻿
--=======================================================================================================

-- Name/Description: Get Table Information for Each DataBase
-- Author: Shaun Salter-Baines
-- Date: 11.12.2018
--=======================================================================================================

CREATE PROCEDURE [sch_database].[TableStats]
@DBName VARCHAR(100)


AS


DECLARE @SQL NVARCHAR(MAX)

SET @SQL = '
USE ' + QUOTENAME(@DBName) +
'
INSERT INTO [DBAMaintenance].[sch_database].[TableDetails] (
	DatabaseName,
	TABLE_NAME,
    USER_SEEKS,
    USER_SCANS,
    USER_LOOKUPS,
    USER_UPDATES,
    LAST_USER_SEEK,
    LAST_USER_SCAN,
    LAST_USER_LOOKUP,
    LAST_USER_UPDATE
)
SELECT  
' + '''' + QUOTENAME(@DBName) + '''' + ' AS DBName
    ,object_name(STAT.object_id) AS ObjectName, 
    USER_SEEKS,
    USER_SCANS,
    USER_LOOKUPS,
    USER_UPDATES,
    LAST_USER_SEEK,
    LAST_USER_SCAN,
    LAST_USER_LOOKUP,
    LAST_USER_UPDATE
FROM  SYS.DM_DB_INDEX_USAGE_STATS STAT JOIN
        SYS.TABLES TAB ON (TAB.OBJECT_ID = STAT.OBJECT_ID)
WHERE   DATABASE_ID = DB_ID()
ORDER BY last_user_seek DESC'

EXEC sp_executesql @SQL


/****** Object:  StoredProcedure [sch_job].[FailedJobs]    Script Date: 29/05/2019 14:41:20 ******/
SET ANSI_NULLS ON