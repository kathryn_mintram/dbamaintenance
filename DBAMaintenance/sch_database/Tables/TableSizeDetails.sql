﻿CREATE TABLE [sch_database].[TableSizeDetails] (
    [DatabaseName]  VARCHAR (150)   NULL,
    [TableName]     VARCHAR (150)   NULL,
    [RowCounts]     VARCHAR (50)    NULL,
    [SpaceInGB]     NUMERIC (36, 2) NULL,
    [UnusedSpaceMB] NUMERIC (36, 2) NULL,
    [DateCaptured]  DATETIME        NULL
);

