﻿CREATE TABLE [sch_database].[MissingDBBackups] (
    [DB Name]     VARCHAR (100) NOT NULL,
    [Type]        VARCHAR (5)   NOT NULL,
    [Last Backup] VARCHAR (100) NULL
);

