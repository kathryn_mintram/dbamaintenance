﻿CREATE TABLE [sch_database].[FileSizeDetails] (
    [id]           INT             IDENTITY (1, 1) NOT NULL,
    [DatabaseName] [sysname]       NOT NULL,
    [LogicalName]  VARCHAR (128)   NOT NULL,
    [PhysicalName] VARCHAR (256)   NOT NULL,
    [SizeMB]       DECIMAL (18, 2) NOT NULL,
    [LogDate]      DATETIME        DEFAULT (getdate()) NOT NULL
);

