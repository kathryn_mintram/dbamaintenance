﻿CREATE TABLE [sch_database].[TableDetails] (
    [DatabaseName]     VARCHAR (150) NULL,
    [TABLE_NAME]       VARCHAR (150) NULL,
    [USER_SEEKS]       BIGINT        NULL,
    [USER_SCANS]       INT           NULL,
    [USER_LOOKUPS]     INT           NULL,
    [USER_UPDATES]     INT           NULL,
    [LAST_USER_SEEK]   DATETIME      NULL,
    [LAST_USER_SCAN]   DATETIME      NULL,
    [LAST_USER_LOOKUP] DATETIME      NULL,
    [LAST_USER_UPDATE] DATETIME      NULL
);

