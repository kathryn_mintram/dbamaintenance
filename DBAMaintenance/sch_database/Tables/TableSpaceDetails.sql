﻿CREATE TABLE [sch_database].[TableSpaceDetails] (
    [Server_Name]   [sysname]       NOT NULL,
    [dbName]        [sysname]       NOT NULL,
    [TableName]     [sysname]       NULL,
    [SchemaName]    [sysname]       NULL,
    [RowCounts]     BIGINT          NULL,
    [TotalSpaceKB]  DECIMAL (20, 1) NULL,
    [TotalSpaceMB]  DECIMAL (20, 1) NULL,
    [UsedSpaceKB]   DECIMAL (20, 1) NULL,
    [UsedSpaceMB]   DECIMAL (20, 1) NULL,
    [UnusedSpaceKB] DECIMAL (20, 1) NULL,
    [UnusedSpaceMB] DECIMAL (20, 1) NULL,
    [Report_Date]   DATETIME        DEFAULT (getdate()) NULL
);

