﻿CREATE TABLE [sch_database].[FileSpaceDetails] (
    [Server_Name] [sysname]       NOT NULL,
    [dbName]      [sysname]       NOT NULL,
    [Flag]        BIT             NULL,
    [Fileid]      TINYINT         NULL,
    [FileGroup]   [sysname]       NULL,
    [Total_Space] DECIMAL (20, 1) NULL,
    [UsedSpace]   DECIMAL (20, 1) NULL,
    [FreeSpace]   DECIMAL (20, 1) NULL,
    [FreePct]     DECIMAL (20, 3) NULL,
    [Name]        VARCHAR (250)   NULL,
    [FileName]    [sysname]       NULL,
    [Report_Date] DATETIME        DEFAULT (getdate()) NULL
);

