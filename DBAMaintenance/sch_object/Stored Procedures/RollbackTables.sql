﻿


---- =============================================
---- Author:		Kathryn Mintram
---- Create date: 04.10.2018
---- Description:	This SP finds any tables with created as part of a rollback plan for changes made through
---- SysAid or Jira and provides information on indexes, pages, and space used by that table.
---- =============================================

CREATE PROCEDURE [sch_object].[RollbackTables] 

AS
BEGIN

--We want to make sure that when we collect the DBs we need to scan through, that we don't include this in our final results
SET NOCOUNT ON;

DECLARE @LoopCounter INT , 
		@MaxCounterId INT, 
        @DBName NVARCHAR(100), 
		@SQL NVARCHAR(MAX), 
		@SQLPRINT NVARCHAR(MAX), 
		@TableName VARCHAR(100)


IF OBJECT_ID('tempdb..#DBNames') IS NOT NULL DROP TABLE #DBNames
IF OBJECT_ID('tempdb..#ObjectResults') IS NOT NULL DROP TABLE #ObjectResults


--Store a list of DBs on the Instance so that we can loop through
SELECT
	[Name]
	 ,ROW_NUMBER() OVER(ORDER BY [Name] ASC) AS row_no
INTO #DBNames
FROM sys.databases
--Make sure that System DBs and anything in a restore state is not included.
WHERE state_desc = 'ONLINE'
AND name NOT IN ('master', 'tempdb', 'model', 'msdb')

SET NOCOUNT ON;

-- Create a Table to store our results
CREATE TABLE #ObjectResults (
	DBName VARCHAR(100) NOT NULL,
	SchemaName VARCHAR(100) NOT NULL,
	TableName VARCHAR(500) NOT NULL,
	CreationDate VARCHAR(11) NOT NULL,
	ModifyDate VARCHAR(11) NOT NULL,
	IndexName VARCHAR(100)  NULL,
	Rows int NOT NULL,
	TotalPages int NOT NULL,
	UsedPages int NOT NULL,
	DataPages int NOT NULL,
	TotalSpaceMB int NOT NULL,
	UsedSpaceMB int NOT NULL,
	DataSpaceMB int NOT NULL, 
	Row_No int NOT NULL
)

--Setup a loop counter 
SELECT @LoopCounter = min(row_no) , @MaxCounterId = max(row_no) 
FROM #DBNames
 
 --Setup where conditions for loop
WHILE(@LoopCounter IS NOT NULL AND @LoopCounter <= @MaxCounterId)

--Complete steps below until while condition is no longer true
BEGIN
   SELECT @DBName = [Name]
   FROM #DBNames WHERE row_no = @LoopCounter
	   SET @SQL = 
	   'INSERT INTO #ObjectResults (DBName, SchemaName, TableName, CreationDate, ModifyDate, IndexName, Rows, TotalPages, UsedPages, DataPages, TotalSpaceMB, UsedSpaceMB, DataSpaceMB, Row_No)
			SELECT ' +
					QUOTENAME(@DBName, '''') + ' AS DBName,
					s.name AS SchemaName,
					t.name AS TableName,
					t.create_date AS CreationDate,
					t.modify_date AS ModifyDate,
					i.name AS indexName,
					p.[Rows],
					sum(a.total_pages) as TotalPages, 
					sum(a.used_pages) as UsedPages, 
					sum(a.data_pages) as DataPages,
					(sum(a.total_pages) * 8) / 1024 as TotalSpaceMB, 
					(sum(a.used_pages) * 8) / 1024 as UsedSpaceMB, 
					(sum(a.data_pages) * 8) / 1024 as DataSpaceMB, 
					ROW_NUMBER() OVER(ORDER BY t.name ASC) AS Row_No
				FROM 
					' + QUOTENAME(@DBName) + '.sys.schemas s
				INNER JOIN
					' + QUOTENAME(@DBName) + '.sys.tables t ON s.schema_id = t.schema_id
				INNER JOIN 
					' + QUOTENAME(@DBName) + '.sys.indexes i ON t.OBJECT_ID = i.object_id
				INNER JOIN 
					' + QUOTENAME(@DBName) + '.sys.partitions p ON i.object_id = p.OBJECT_ID AND i.index_id = p.index_id
				INNER JOIN 
					' + QUOTENAME(@DBName) + '.sys.allocation_units a ON p.partition_id = a.container_id
				WHERE 
					t.Name LIKE ''%[0-9]%%''
					and t.type = ''u''
				AND 
					t.Name NOT IN (''i18nTranslations'', ''EndecaSlugsV6'', ''V12Refunds'', ''V12FinanceAudit'', ''GoogleCampaign2'',
					''Jpmc3DSecureErrorLog'',''MatchCode360Audit'',''StockAmendLogin100'',''tActiveLast120'', ''ComCode8Upload'', ''ProductMedia360'', ''Products_2'',
					''ProductDescriptionV12Options'', ''V12Options'', ''i18nCategories'', ''i18nText'', ''EndecaSlugsV6_1'', ''EndecaSlugsV5'')
				AND
					i.OBJECT_ID > 255 AND 
					i.index_id <= 1
				GROUP BY 
					s.name, t.NAME, t.create_date, t.modify_date, i.object_id, i.index_id, i.name, p.[Rows]
				ORDER BY 
					object_name(i.object_id)'
		EXEC sys.sp_executesql @SQL
   SET @LoopCounter  = @LoopCounter  + 1        
END

--Return our result set
SELECT * FROM #ObjectResults
END


/****** Object:  StoredProcedure [sch_object].[SPUsage]    Script Date: 29/05/2019 14:52:25 ******/
SET ANSI_NULLS ON