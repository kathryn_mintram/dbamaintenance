﻿
-- =============================================
-- Author:		Shaun Salter-Baines
-- Create date: 13.11.2017
-- Description:	This SP finds all objects that have recently been created against all DBs - primarily built to measure how many Objects
-- we need to Test for when running SQL reviews - however this can also enable us to check on Production if anything has recently been modified.
-- =============================================

CREATE PROCEDURE [sch_object].[RecentCreatedObjects] 


AS
BEGIN


DECLARE 
@LoopCounter INT , 
@MaxCounterId INT, 
@DBName NVARCHAR(100), 
@ObjectName NVARCHAR(100), 
@SQL NVARCHAR(MAX), 
@SQL1 NVARCHAR(MAX)


IF OBJECT_ID('tempdb..#DBNames') IS NOT NULL DROP TABLE #DBNames
IF OBJECT_ID('tempdb..#ObjectResults') IS NOT NULL DROP TABLE #ObjectResults

--We want to make sure that when we collect the DBs we need to scan through, that we don't include this in our final results
SET NOCOUNT ON;

SELECT
[Name]
 ,ROW_NUMBER() OVER(ORDER BY [Name] ASC) AS row_no
INTO #DBNames
FROM sys.databases
--Make sure that System DBs and anything in a restore state is not included.
WHERE state_desc = 'ONLINE'
AND name NOT IN ('master', 'tempdb', 'model', 'msdb')


-- Create a Table to store our results
CREATE TABLE #ObjectResults (
DBName VARCHAR(100) NOT NULL,
Name VARCHAR(500) NOT NULL,
Type_DESC VARCHAR(100) NOT NULL,
Create_Date DATETIME NOT NULL,
Modify_Date DATETIME NOT NULL
)


SELECT @LoopCounter = min(row_no) , @MaxCounterId = max(row_no) 
FROM #DBNames

 
WHILE(@LoopCounter IS NOT NULL
      AND @LoopCounter <= @MaxCounterId)
BEGIN
   SELECT @DBName = [Name]
   FROM #DBNames WHERE row_no = @LoopCounter
    --SET @SQL = 'SELECT COUNT(0) FROM ' + QUOTENAME(@DBName) + '.sys.objects WHERE (create_date > DATEADD(HOUR, -1, GETDATE()) OR modify_date > DATEADD(HOUR, -1, GETDATE()))'
   SET @SQL = 'INSERT INTO #ObjectResults (DBName, Name, Type_DESC, Create_Date, Modify_Date) 
							SELECT ' + QUOTENAME(@DBName, '''')+ ' AS DBName ' + ', Name, type_desc, create_date, modify_date  
							FROM ' + QUOTENAME(@DBName) + '.sys.objects 
							WHERE (create_date > DATEADD(HOUR, -1, GETDATE()) OR modify_date > DATEADD(HOUR, -1, GETDATE()))'
   EXEC sys.sp_executesql @SQL
   SET @LoopCounter  = @LoopCounter  + 1        
END


-- Return our result set
SELECT * FROM #ObjectResults


END


/****** Object:  StoredProcedure [sch_object].[RollbackTables]    Script Date: 29/05/2019 14:51:56 ******/
SET ANSI_NULLS ON