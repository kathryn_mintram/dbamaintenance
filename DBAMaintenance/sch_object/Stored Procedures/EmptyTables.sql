﻿
--=======================================================================================================

-- Name/Description: Get Table Information for Each DataBase
-- Author: Shaun Salter-Baines
-- Date: 11.12.2018
--=======================================================================================================

CREATE PROCEDURE [sch_object].[EmptyTables]
@DBName VARCHAR(100)


AS


-- Create CTE for the unused tables, which are the tables from the sys.all_objects and 
-- not in the sys.dm_db_index_usage_stats table

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = '
USE ' + QUOTENAME(@DBName) +
'

;WITH UnUsedTables (TableName , TotalRowCount, CreatedDate , LastModifiedDate ) 
AS 
( 
  SELECT 
	 DBTable.name AS TableName
     ,PS.row_count AS TotalRowCount
     ,DBTable.create_date AS CreatedDate
     ,DBTable.modify_date AS LastModifiedDate
  FROM sys.all_objects  DBTable 
     JOIN sys.dm_db_partition_stats PS ON OBJECT_NAME(PS.object_id) = DBTable.name
  WHERE DBTable.type = ''U'' 
     AND NOT EXISTS (SELECT OBJECT_ID  
                     FROM sys.dm_db_index_usage_stats
                     WHERE OBJECT_ID = DBTable.object_id )
)
INSERT INTO [DBAMaintenance].[sch_object].[ZeroRowCountTables] (DatabaseName, TableName, TotalRowCount, CreatedDate, LastModifiedDate, DateCaptured)
SELECT ' + '''' + QUOTENAME(@DBName) + '''' + ',TableName , TotalRowCount, CreatedDate , LastModifiedDate, GETDATE()
FROM UnUsedTables
WHERE TotalRowCount  = 0
ORDER BY TotalRowCount ASC
'


EXEC sp_executesql @SQL


/****** Object:  StoredProcedure [sch_object].[FindColumn]    Script Date: 29/05/2019 14:49:33 ******/
SET ANSI_NULLS ON