﻿------------------------------------------------------------------------------------------------------------------------------
-- [Object].[UnusedSPs] 
-- Purpose: Checks for potentially unused stored procedures.  
--
-- Date         Who							Version		Ticket			Comments			
-- 10/05/2019	Kathryn Mintram				1.00		-				Initial Write 
------------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [sch_object].[UnusedSPs] 

AS
BEGIN

--Declare variables
DECLARE @Loopcounter INT
		,@MaxCounterId int
		,@DBName NVARCHAR(128)
		,@SQL NVARCHAR(MAX)

--Drop temporary tables if they already exist
IF OBJECT_ID('tempdb..#DBNames') IS NOT NULL DROP TABLE #DBNames
IF OBJECT_ID('tempdb..#Results') IS NOT NULL DROP TABLE #Results

--Store a list of DBs on the Instance so that we can loop through
SELECT[Name]
	 ,ROW_NUMBER() OVER(ORDER BY [Name] ASC) AS row_no
INTO #DBNames
FROM sys.databases
--Make sure that System DBs and anything in a restore state is not included.
WHERE state_desc = 'ONLINE'
AND name NOT IN ('master', 'tempdb', 'model', 'msdb', 'ReportServer', 'ReportServerTempDB', 'SSISDB')
AND name NOT LIKE '%_ETL'
AND name NOT LIKE '%_Yesterday'

CREATE TABLE #Results (
			DBName sysname,
			SPName sysname)

--Setup a loop counter 
SELECT @LoopCounter = min(row_no) 
		,@MaxCounterId = max(row_no) 
FROM #DBNames
 
 --Setup where conditions for loop
WHILE(@LoopCounter IS NOT NULL AND @LoopCounter <= @MaxCounterId)

--Complete steps below until while condition is no longer true
	BEGIN
	   SELECT @DBName = [Name]
	   FROM #DBNames WHERE row_no = @LoopCounter
		   SET @SQL = 
		   'INSERT INTO #Results (DBName, SPName)
			SELECT ' + QUOTENAME(@DBName, '''') + ',
					name collate SQL_Latin1_General_CP1_CI_AS
			FROM ' + QUOTENAME(@DBName) + '.sys.objects
			WHERE type = ''P''
			EXCEPT
			SELECT ' + QUOTENAME(@DBName, '''') + ',
			ObjectName
			FROM [DBAMaintenance].[Object].[SPUsageDetails]
			WHERE DataBaseName = ' + QUOTENAME(@DBName, '''') + ''
			PRINT @SQL
			EXEC sp_executesql @SQL
	   SET @LoopCounter  = @LoopCounter  + 1        
	END


SELECT * 
FROM #Results
ORDER BY DBName, SPName

END