﻿

------------------------------------------------------------------------------------------------------------------------------
-- [Object].[IndexUsage] 
-- Purpose: Checks for potentially unused indexes.  
-- https://blog.sqlauthority.com/2011/01/04/sql-server-2008-unused-index-script-download/
--
-- Date         Who							Version		Ticket			Comments			
-- 10/05/2019	Kathryn Mintram				1.00		-				Initial Write 
------------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [sch_object].[IndexUsage] 

AS
BEGIN
--Declare variables
DECLARE @Loopcounter INT
		,@MaxCounterId int
		,@DBName NVARCHAR(128)
		,@SQL NVARCHAR(MAX)

--Drop temporary tables if they already exist
IF OBJECT_ID('tempdb..#DBNames') IS NOT NULL DROP TABLE #DBNames
IF OBJECT_ID('tempdb..#Indexes') IS NOT NULL DROP TABLE #Indexes

--Store a list of DBs on the Instance so that we can loop through
SELECT[Name]
	 ,ROW_NUMBER() OVER(ORDER BY [Name] ASC) AS row_no
INTO #DBNames
FROM sys.databases
--Make sure that System DBs and anything in a restore state is not included.
WHERE state_desc = 'ONLINE'
AND name NOT IN ('master', 'tempdb', 'model', 'msdb', 'ReportServer', 'ReportServerTempDB', 'SSISDB')

--CREATE TABLE #Indexes (
--        [DBname] sysname
--        ,[ObjectName] sysname
--        ,[IndexName] sysname
--		,[IndexID] int
--		,[UserSeeks] bigint
--		,[UserScans] bigint
--		,[UserLookups] bigint
--		,[UserUpdates] bigint
--		,[TableRows] bigint
--        )

--Setup a loop counter 
SELECT @LoopCounter = min(row_no) 
		,@MaxCounterId = max(row_no) 
FROM #DBNames
 
 --Setup where conditions for loop
WHILE(@LoopCounter IS NOT NULL AND @LoopCounter <= @MaxCounterId)

--Complete steps below until while condition is no longer true
BEGIN
   SELECT @DBName = [Name]
   FROM #DBNames WHERE row_no = @LoopCounter
	   SET @SQL = 
	   'INSERT INTO [sch_object].[IndexUsageDetails] (DBName, ObjectName, IndexName, IndexID, UserSeeks, UserScans, UserLookups, UserUpdates, TableRows)
		SELECT ' +
					QUOTENAME(@DBName, '''') + ',
			   o.name AS ObjectName,
			   i.name AS IndexName,
			   i.index_id AS IndexID,
			   dm_ius.user_seeks AS UserSeek,
			   dm_ius.user_scans AS UserScans,
			   dm_ius.user_lookups AS UserLookups,
			   dm_ius.user_updates AS UserUpdates,
			   p.TableRows
		FROM ' + QUOTENAME(@DBName) + '.sys.dm_db_index_usage_stats dm_ius
			INNER JOIN ' + QUOTENAME(@DBName) + '.sys.indexes i
				ON i.index_id = dm_ius.index_id
				   AND dm_ius.object_id = i.object_id
			INNER JOIN ' + QUOTENAME(@DBName) + '.sys.objects o
				ON dm_ius.object_id = o.object_id
			INNER JOIN ' + QUOTENAME(@DBName) + '.sys.schemas s
				ON o.schema_id = s.schema_id
			INNER JOIN
			(SELECT SUM(p.rows) TableRows,
					   p.index_id,
					   p.object_id
				FROM ' + QUOTENAME(@DBName) + '.sys.partitions p
				GROUP BY p.index_id,
						 p.object_id) p
				ON p.index_id = dm_ius.index_id
				   AND dm_ius.object_id = p.object_id
		WHERE dm_ius.database_id = DB_ID(' + QUOTENAME(@DBName, '''') + ')
			  AND i.type_desc = ''nonclustered''
			  AND i.is_primary_key = 0
			  AND i.is_unique_constraint = 0
			  AND dm_ius.user_seeks = 0
			  AND dm_ius.user_scans = 0
			  AND dm_ius.user_lookups = 0
			  AND dm_ius.user_updates <> 0'
		EXEC sp_executesql @SQL
   SET @LoopCounter  = @LoopCounter  + 1        
END

--Return our result set
--SELECT DBname
--		,ObjectName
--		,IndexName
--		,IndexID
--		,UserSeeks
--		,UserScans
--		,UserLookups
--		,UserUpdates
--		,TableRows
--FROM #Indexes
--ORDER BY DBname

END


/****** Object:  StoredProcedure [sch_object].[RecentCreatedObjects]    Script Date: 29/05/2019 14:51:32 ******/
SET ANSI_NULLS ON