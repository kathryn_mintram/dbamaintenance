﻿

--=======================================================================================================

-- Name/Description: Get Table Information for a specified DB
-- Author: Shaun Salter-Baines - Gets information of when the Table last had data written into it, however this is not statistical information as the DB might not have been active
-- Date: 02.08.2018
--=======================================================================================================

CREATE PROCEDURE [sch_object].[TableLatestDates]
@DBName VARCHAR(100)

AS
BEGIN

DECLARE @SQL NVARCHAR(MAX),
              @LoopCounter INT , 
              @MaxCounterId INT,
              @StatementToExecute NVARCHAR(MAX),
              @TableName VARCHAR(100)

IF OBJECT_ID('tempdb..#StatementToExecute') IS NOT NULL DROP TABLE #StatementToExecute


CREATE TABLE #StatementToExecute (TableName VARCHAR(250), SelectStatement VARCHAR(1000), row_no INT)


SET @SQL  = 
' USE ' + @DBName +
'
INSERT INTO #StatementToExecute (TableName, SelectStatement, row_no)
SELECT
TABLE_NAME,
'',* FROM ''  + TABLE_SCHEMA + ''.'' + TABLE_NAME + '' ORDER BY '' +
COLUMN_NAME + '' DESC'',
ROW_NUMBER() OVER(ORDER BY COLUMN_NAME ASC) AS row_no
FROM INFORMATION_SCHEMA.COLUMNS
WHERE DATA_TYPE = ''datetime'''

EXEC sp_executesql @SQL


SELECT @LoopCounter = min(row_no) , @MaxCounterId = max(row_no) 
FROM #StatementToExecute


WHILE(@LoopCounter IS NOT NULL AND @LoopCounter <= @MaxCounterId)
BEGIN
   SELECT 
   @TableName = TableName,
   @StatementToExecute = SelectStatement
   FROM #StatementToExecute WHERE row_no = @LoopCounter
       SET @SQL = 'USE ' + @DBName + ' ' + 'SELECT TOP 1 ' + '''' + @TableName + '''' + ' AS TableName' + @StatementToExecute
       PRINT @SQL
   EXEC sp_executesql @SQL
   SET @LoopCounter  = @LoopCounter  + 1        
END


END