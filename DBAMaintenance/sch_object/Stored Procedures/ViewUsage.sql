﻿





------------------------------------------------------------------------------------------------------------------------------
-- [Object].[ViewUsage] 
-- Purpose: Checks for the name of views in the plan cache and records any in a table. 
-- Over time this data can be used to identify unused views. 
--
-- Date         Who							Version		Ticket			Comments			
-- 27/03/2018	Kathryn Mintram				1.00		DBACLN-7		Initial Write 
------------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [sch_object].[ViewUsage] 

AS

DECLARE @LoopCounter int,
		@MaxCounter int,
		@SQL nvarchar(max),
		@DBName nvarchar(32)

IF OBJECT_ID('tempdb..#DBNames') IS NOT NULL DROP TABLE #DBNames
IF OBJECT_ID('tempdb..#Results') IS NOT NULL DROP TABLE #Results

--Store a list of databases to run the query against
CREATE TABLE #DBNames (
	DBName nvarchar(32),
	RowNo int)

INSERT INTO #DBNames
VALUES 
('WiggleBI', 1),
('Wiggle-Reporting', 2),
('Reports',3)

--Create a table to store the results
CREATE TABLE #Results (
	DBName nvarchar(32),
	ViewName nvarchar(128))

--Setup a loop counter 
SELECT @LoopCounter = min(RowNo) , @MaxCounter = max(RowNo) 
FROM #DBNames
 
 --Setup where conditions for loop
WHILE(@LoopCounter IS NOT NULL AND @LoopCounter <= @MaxCounter)

--Complete steps below until while condition is no longer true
BEGIN
   SELECT @DBName = [DBName]
   FROM #DBNames 
   WHERE RowNo = @LoopCounter
	   SET @SQL = 'INSERT INTO #Results (DBName, ViewName)
					SELECT ' + QUOTENAME(@DBName, '''') + ' 
							,v.name [ViewName]
					FROM ' + QUOTENAME(@DBName) + '.sys.views v
					WHERE v.is_ms_shipped = 0
					EXCEPT 
					SELECT ' + QUOTENAME(@DBName, '''') + ' 
							,o.Name [ViewName]
					FROM master.sys.dm_exec_cached_plans p
					CROSS APPLY ' + QUOTENAME(@DBName) + '.sys.dm_exec_sql_text(p.plan_handle) t
					INNER JOIN ' + QUOTENAME(@DBName) + '.sys.objects o ON t.objectid = o.object_id
					WHERE t.dbid = DB_ID()'
		EXEC sp_executesql @SQL
   SET @LoopCounter  = @LoopCounter  + 1    
END

IF (NOT EXISTS(SELECT DBName, ViewName FROM [DBAMaintenance].[sch_object].[ViewUsageDetails]))
BEGIN
	INSERT INTO [DBAMaintenance].[sch_object].[ViewUsageDetails] (DBName, ViewName)
	SELECT * 
	FROM #Results
END

DROP TABLE #DBNames
DROP TABLE #Results


/****** Object:  StoredProcedure [sch_security].[DBUsers]    Script Date: 29/05/2019 14:53:30 ******/
SET ANSI_NULLS ON