﻿
-- =============================================
-- Author:		Shaun Salter-Baines
-- Create date: 13.11.2017
-- Description:	This SP finds a Column within a Table/Sp/View for you and its DataType, please note that this is for a single DB and requires
-- you to run against DBs by Using the Use Statement.
-- Example Find_Column @ColumnName, @ObjectType - note that object type if omitted resolves to U Type
-- =============================================


CREATE PROCEDURE [sch_object].[FindColumn] 
	@ColumnName VARCHAR(100) = NULL, @ObjectType VARCHAR(3) = 'U'
	

AS
BEGIN

--We want to make sure that when we collect the DBs we need to scan through, that we don't include this in our final results
SET NOCOUNT ON;


DECLARE @LoopCounter INT , 
		@MaxCounterId INT, 
        @DBName NVARCHAR(100), 
		@SQL NVARCHAR(MAX), 
		@SQL1 NVARCHAR(MAX)


--Add wildcards to the Var
SET @ColumnName = '''%'  + @ColumnName + '%'''


IF OBJECT_ID('tempdb..#DBNames') IS NOT NULL DROP TABLE #DBNames
IF OBJECT_ID('tempdb..#ObjectResults') IS NOT NULL DROP TABLE #ObjectResults


--Store a list of DBs on the Instance so that we can loop through
SELECT
	[Name]
	 ,ROW_NUMBER() OVER(ORDER BY [Name] ASC) AS row_no
INTO #DBNames
FROM sys.databases
--Make sure that System DBs and anything in a restore state is not included.
WHERE state_desc = 'ONLINE'
AND name NOT IN ('master', 'tempdb', 'model', 'msdb')

SET NOCOUNT OFF;

-- Create a Table to store our results
CREATE TABLE #ObjectResults (
	DBName VARCHAR(100) NOT NULL,
	ColumnName VARCHAR(500) NOT NULL,
	DataType VARCHAR(100) NOT NULL,
	ObjectName VARCHAR(200) NOT NULL
)


SELECT @LoopCounter = min(row_no) , @MaxCounterId = max(row_no) 
FROM #DBNames

 
WHILE(@LoopCounter IS NOT NULL AND @LoopCounter <= @MaxCounterId)
BEGIN
   SELECT @DBName = [Name]
   FROM #DBNames WHERE row_no = @LoopCounter
   SET @SQL = 
			   'INSERT INTO #ObjectResults (DBName, ColumnName, DataType, ObjectName)
			   SELECT' +
					QUOTENAME(@DBName, '''') + ' AS DBName,
					col.name AS ColumnName,
					DataType.name As DataType,
					obj.name AS ObjectName
				FROM ' + QUOTENAME(@DBName) + '.SYS.columns col
				INNER JOIN ' + QUOTENAME(@DBName) + '.SYS.objects obj
					ON col.object_id = obj.object_id
				INNER JOIN ' + QUOTENAME(@DBName) + '.sys.types DataType
					ON Col.user_type_id = DataType.user_type_id 
				WHERE col.name LIKE ' + @ColumnName + ' 
				AND obj.type  = ' + QUOTENAME(@ObjectType, '''')
   EXEC sp_executesql @SQL
   SET @LoopCounter  = @LoopCounter  + 1        
END


-- Return our result set
SELECT * FROM #ObjectResults


END


/****** Object:  StoredProcedure [sch_object].[FindObject]    Script Date: 29/05/2019 14:50:05 ******/
SET ANSI_NULLS ON