﻿
--=======================================================================================================

-- Name/Description: Get Table Information for Each DataBase
-- Author: Shaun Salter-Baines
-- Date: 11.12.2018
--=======================================================================================================

CREATE PROCEDURE [sch_object].[SPUsage]
@DBName VARCHAR(100)


AS

-- Create CTE for the unused tables, which are the tables from the sys.all_objects and 
-- not in the sys.dm_db_index_usage_stats table

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = '
USE ' + QUOTENAME(@DBName) +
'
INSERT INTO [DBAMaintenance].[sch_object].[SPUsageDetails] (DataBaseName, ObjectName, TypeDesc, Last_Execution_Time, ExecutionCount)
SELECT
 DB_NAME(eps.database_ID) AS DatabaseName
 ,OBJECT_NAME(eps.object_ID) AS ObjectName
 ,eps.type_desc
 ,eps.last_execution_time
 ,eps.execution_count 
FROM sys.dm_exec_procedure_stats eps
WHERE database_id = DB_ID()
'

EXEC sp_executesql @SQL


/****** Object:  StoredProcedure [sch_object].[ViewUsage]    Script Date: 29/05/2019 14:53:07 ******/
SET ANSI_NULLS ON