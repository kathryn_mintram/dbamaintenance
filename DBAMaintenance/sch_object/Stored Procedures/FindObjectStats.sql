﻿


-- =============================================
-- Author:		Shaun Salter-Baines
-- Create date: 10.08.2018
-- Description:	This SP finds a Column, Table or SP and information on that Object
-- =============================================

CREATE PROCEDURE [sch_object].[FindObjectStats] @Txt VARCHAR(1000)


AS
BEGIN


DECLARE
@ObjectType VARCHAR(250),
@ObjectName VARCHAR(250),
@SPObjectID INT


IF OBJECT_ID('tempdb..#ObjectResults') IS NOT NULL DROP TABLE #ObjectResults


--Identify the Object Type and what Schema it belongs to
SELECT DISTINCT
	SC.Name as SchemaName,
	T.NAME AS OBJECTNAME, 
	C.NAME AS COLUMNNAME, 
	--P.NAME AS [DATA TYPE], 
	--P.MAX_LENGTH AS[SIZE],
	CASE WHEN C.NAME = @Txt
		THEN 'Column'
	WHEN T.Type = 'AF'
		THEN 'Aggregate function (CLR)'
	WHEN T.Type = 'C'
		THEN 'CHECK constraint'
	WHEN T.Type = 'D'
		THEN 'DEFAULT (constraint or stand-alone)'
	WHEN T.Type = 'F'
		THEN 'FOREIGN KEY constraint' 
	WHEN T.Type = 'FN'
		THEN 'SQL scalar function'
	WHEN T.Type = 'FS'
		THEN 'Assembly (CLR) scalar-function'
	WHEN T.Type = 'FT'
		THEN 'Assembly (CLR) table-valued function'
	WHEN T.Type = 'IF' 
		THEN 'SQL inline table-valued function'
	WHEN T.Type = 'IT' 
		THEN 'Internal table'
	WHEN Type = 'P'
		THEN 'SQL Stored Procedure'
	WHEN T.Type = 'PC' 
		THEN 'Assembly (CLR) stored-procedure'
	WHEN T.Type = 'PG' 
		THEN 'Plan guide'
	WHEN T.Type = 'PK' 
		THEN 'PRIMARY KEY constraint'
	WHEN T.Type = 'R'   
		THEN 'Rule (old-style, stand-alone)'
	WHEN T.Type = 'RF'
		THEN 'Replication-filter-procedure'
	WHEN T.Type = 'S'  
		THEN 'System base table'
	WHEN T.Type = 'SN' 
		THEN 'Synonym'
	WHEN T.Type = 'SQ' 
		THEN 'Service queue'
	WHEN T.Type = 'TA' 
		THEN 'Assembly (CLR) DML trigger'
	WHEN T.Type = 'TF' 
		THEN 'SQL table-valued-function'
	WHEN T.Type = 'TR' 
		THEN 'SQL DML trigger'
	WHEN T.Type = 'TT' 
		THEN 'Table type'
	WHEN T.Type = 'U'
		THEN 'Table (user-defined)'
	WHEN T.Type = 'UQ' 
		THEN 'UNIQUE constraint'
	WHEN T.Type = 'V' 
		THEN 'View'
	WHEN T.Type = 'X'
		THEN 'Extended stored procedure'
	END AS ObjectType
INTO #ObjectResults
FROM SYS.OBJECTS AS T
INNER JOIN SYS.COLUMNS AS C
	ON T.OBJECT_ID = C.OBJECT_ID
--INNER JOIN SYS.TYPES AS P
--	ON C.SYSTEM_TYPE_ID = P.SYSTEM_TYPE_ID
INNER JOIN sys.schemas SC
	ON SC.schema_id = T.schema_id
--Search on Object or Column
WHERE (C.NAME = @Txt OR T.Name = @Txt)


SELECT * FROM #ObjectResults


SET @ObjectType = (SELECT ObjectType FROM #ObjectResults)

IF @ObjectType IN ('Column', 'Table (user-defined)')
	BEGIN
	SET @ObjectName = (SELECT OBJECTNAME FROM #ObjectResults)


	--Now get stats on the Table, when it was last accessed and read
	DECLARE @accesscount int
	SET @accesscount = 0

	SELECT DB_NAME(0) AS dbname,  
		t.name AS tablename,
		SUM(user_seeks) AS sum_user_seeks, 
		SUM(user_scans) AS sum_user_scans, 
		SUM(user_lookups) AS sum_user_lookups, 
		SUM(user_updates) AS sum_user_updates,
		MAX(last_user_seek) AS last_user_seek, 
		MAX(last_user_scan) AS last_user_scan, 
		MAX(last_user_lookup) AS last_user_lookup, 
		MAX(last_user_update) AS last_user_update
	-- Limit to user tables. System tables
	-- are not referenced in sys.tables.
	FROM sys.tables t 
	-- The stats table tracks all use of a table's indexes
	-- The LEFT OUTER JOINS include tables for which no
	-- statistics have been captured.
	 LEFT OUTER JOIN sys.dm_db_index_usage_stats s
	   ON s.object_id = t.object_id
	 LEFT OUTER JOIN sys.indexes i 
	  ON t.object_id = i.object_id
	  AND s.index_id = i.index_id
	-- Any update indicates use of the table.
	-- Any seeks, scans or lookups indicate table use.
	-- But system functions also can scan the data. 
	-- Any activity means not a candidate
	-- Treat NULL values as 0 for reporting purposes
	WHERE  t.Name = @ObjectName
	AND COALESCE (user_updates,0) = 0 
	AND COALESCE (user_seeks + user_scans + user_lookups,0) <= @accesscount 
	AND COALESCE(s.database_id, DB_ID()) = DB_ID()
	-- Aggregate all index counts for access to a table
	GROUP BY DB_NAME(s.database_id), db_name(s.database_id), t.name
	---- Order the results by database and table name
	ORDER BY dbname, tablename

	END


IF @ObjectType = ('SQL Stored Procedure')
	BEGIN 

	SET @ObjectName = (SELECT OBJECTNAME FROM #ObjectResults)

	EXEC [sch_object].[FindSPStats] @ObjectName

	END


--Finally is this SP/Table/Column referenced in any Jobs or SPs
SELECT
	'JOBs that reference this Object' AS Information,
	j.job_id,
	j.name,
	js.step_id,
	js.command,
	j.enabled 
FROM	msdb.dbo.sysjobs j
JOIN	msdb.dbo.sysjobsteps js
	ON	js.job_id = j.job_id 
WHERE	js.command LIKE '%' + @Txt + '%' -- replace keyword with the word or stored proc that you are searching for


--Finally Are there any SPs, Views or Functions associated with this field or Data? 
SELECT
	SO.Name AS SPs_That_Reference_This_Field_Or_Table,
	SO.type
FROM syscomments sc
LEFT JOIN sysobjects so ON sc.id=so.id
WHERE sc.TEXT LIKE '%' + @Txt + '%'


END


/****** Object:  StoredProcedure [sch_object].[IndexUsage]    Script Date: 29/05/2019 14:51:08 ******/
SET ANSI_NULLS ON