﻿CREATE TABLE [sch_object].[RedundantIndexRecords] (
    [DatabaseName] [sysname]     NOT NULL,
    [TableName]    [sysname]     NOT NULL,
    [IndexName]    [sysname]     NOT NULL,
    [Reason]       NVARCHAR (20) NOT NULL
);

