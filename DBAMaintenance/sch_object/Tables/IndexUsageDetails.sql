﻿CREATE TABLE [sch_object].[IndexUsageDetails] (
    [DBname]      [sysname] NOT NULL,
    [ObjectName]  [sysname] NULL,
    [IndexName]   [sysname] NULL,
    [IndexID]     INT       NULL,
    [UserSeeks]   BIGINT    NOT NULL,
    [UserScans]   BIGINT    NOT NULL,
    [UserLookups] BIGINT    NOT NULL,
    [UserUpdates] BIGINT    NOT NULL,
    [TableRows]   BIGINT    NULL,
    [DateLogged]  DATETIME  DEFAULT (getdate()) NOT NULL
);

