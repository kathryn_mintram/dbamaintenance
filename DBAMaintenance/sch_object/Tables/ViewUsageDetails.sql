﻿CREATE TABLE [sch_object].[ViewUsageDetails] (
    [DBName]     NVARCHAR (32)  NOT NULL,
    [ViewName]   NVARCHAR (128) NOT NULL,
    [DateLogged] DATETIME       DEFAULT (getdate()) NOT NULL
);

