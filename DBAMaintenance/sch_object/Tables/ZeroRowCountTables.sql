﻿CREATE TABLE [sch_object].[ZeroRowCountTables] (
    [DatabaseName]     VARCHAR (150) NULL,
    [TableName]        VARCHAR (150) NULL,
    [TotalRowCount]    INT           NULL,
    [CreatedDate]      DATETIME      NULL,
    [LastModifiedDate] DATETIME      NULL,
    [DateCaptured]     DATETIME      NULL
);

