﻿CREATE TABLE [sch_object].[SPUsageDetails] (
    [DataBaseName]        VARCHAR (150) NULL,
    [ObjectName]          VARCHAR (250) NULL,
    [TypeDesc]            VARCHAR (100) NULL,
    [Last_Execution_Time] DATETIME      NULL,
    [ExecutionCount]      INT           NULL
);

