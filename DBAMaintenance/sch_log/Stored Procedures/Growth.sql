﻿
---- =============================================
---- Author:		Shaun Salter-Baines
---- Create date: 16.08.2018
---- Description:	Monitors log growth, script obtained from here:
---- https://www.mssqltips.com/sqlservertip/1178/monitoring-sql-server-database-transaction-log-space/
---- =============================================

CREATE PROCEDURE [sch_log].[Growth] 

AS


SET NOCOUNT ON 

CREATE TABLE #tFileList 
( 
   databaseName sysname, 
   logSize decimal(18,5), 
   logUsed decimal(18,5), 
   status INT 
) 

INSERT INTO #tFileList 
       EXEC [sch_misc].[ExecDBCCSQLPerf] 

INSERT INTO [sch_log].[SpaceStats] (databaseName, logSize, logUsed) 
SELECT databasename, logSize, logUsed 
FROM #tFileList 

DROP TABLE #tFileList


/****** Object:  StoredProcedure [sch_memory].[BufferCache]    Script Date: 29/05/2019 14:42:15 ******/
SET ANSI_NULLS ON