﻿CREATE TABLE [sch_security].[ADUsers] (
    [Name]                  NVARCHAR (MAX) NULL,
    [Enabled]               NVARCHAR (MAX) NULL,
    [LastLogonDate]         NVARCHAR (MAX) NULL,
    [AccountExpirationDate] NVARCHAR (MAX) NULL
);

