﻿CREATE TABLE [sch_security].[LoginPermissions] (
    [DBName]           NVARCHAR (60)  NULL,
    [UserName]         NVARCHAR (128) NULL,
    [UserType]         NVARCHAR (60)  NULL,
    [DatabaseUserName] NVARCHAR (60)  NOT NULL,
    [Role]             NVARCHAR (60)  NULL,
    [PermissionType]   NVARCHAR (128) NULL,
    [PermissionState]  NVARCHAR (60)  NULL,
    [ObjectType]       NVARCHAR (60)  NULL,
    [ObjectName]       NVARCHAR (128) NULL,
    [ColumnName]       NVARCHAR (60)  NULL
);

