﻿CREATE TABLE [sch_security].[GDPRPCITablePrivileges] (
    [DBName]           NVARCHAR (100) NULL,
    [TableOwner]       NVARCHAR (20)  NULL,
    [TableName]        NVARCHAR (100) NULL,
    [Grantor]          NVARCHAR (20)  NULL,
    [UserWhoHasAccess] NVARCHAR (100) NULL,
    [Privilege]        NVARCHAR (100) NULL,
    [IsGrantable]      NVARCHAR (3)   NULL
);

