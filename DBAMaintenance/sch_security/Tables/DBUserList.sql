﻿CREATE TABLE [sch_security].[DBUserList] (
    [Login]       [sysname]      NULL,
    [HostName]    [sysname]      NULL,
    [DBName]      [sysname]      NULL,
    [ProgramName] VARCHAR (1000) NULL,
    [DateLogged]  DATETIME       CONSTRAINT [DF__DBUserLis__DateL__4277DAAA] DEFAULT (getdate()) NOT NULL
);

