﻿CREATE TABLE [sch_security].[GDPRPCITableCatalogue] (
    [DBName]      VARCHAR (100) NOT NULL,
    [SchemaName]  VARCHAR (100) NOT NULL,
    [TableName]   VARCHAR (200) NOT NULL,
    [ColumnName]  VARCHAR (500) NOT NULL,
    [DataType]    VARCHAR (100) NOT NULL,
    [PCI_Or_GDPR] VARCHAR (30)  NOT NULL
);

