﻿


------------------------------------------------------------------------------------------------------------------------------
-- [Security].[GroupMembers]
-- Purpose: Grabs all members within a Windows Group. Stored for any future investigative reasons and for use in other procedures
--
-- Date         Who							Version		Comments
-- 20/03/2018	Kathryn Mintram				1.00		Initial Write 
------------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [sch_security].[GroupMembers] 

AS

DECLARE @LoopCounter int
		,@MaxCounter int
		,@GroupName nvarchar(64)
		,@SQL nvarchar(max)

IF OBJECT_ID('tempdb..#Groups') IS NOT NULL DROP TABLE #Groups

CREATE TABLE #Groups (
	GroupName sysname
	,RowNo int)

INSERT INTO #Groups
SELECT name
		,ROW_NUMBER() OVER(ORDER BY [name] ASC) 
FROM sys.server_principals
WHERE type = 'G'
AND name NOT LIKE 'NT%'

--Setup a loop counter 
SELECT @LoopCounter = min(RowNo) , @MaxCounter = max(RowNo) 
FROM #Groups
 
 --Setup where conditions for loop
WHILE(@LoopCounter IS NOT NULL AND @LoopCounter <= @MaxCounter)

--Complete steps below until while condition is no longer true
BEGIN
   SELECT @GroupName = GroupName
   FROM #Groups 
   WHERE RowNo = @LoopCounter
	   SET @SQL = 
	   'INSERT INTO [DBAMaintenance].[sch_security].[GroupMembersList] (UserName, UserType, Privilege, MappedLogin, GroupName)
			EXEC xp_logininfo ' + QUOTENAME(@GroupName, '''') + ' , ''members'''
		EXEC sp_executesql @SQL
   SET @LoopCounter  = @LoopCounter  + 1        
END

DROP TABLE #Groups


/****** Object:  StoredProcedure [sch_security].[LoginsandPermissions]    Script Date: 29/05/2019 14:57:33 ******/
SET ANSI_NULLS ON