﻿--USE master;
--IF OBJECT_ID ('script_login') IS NOT NULL DROP PROCEDURE script_login
--GO
CREATE PROCEDURE [sch_security].[ScriptSingleLogin]
(
 @login_name sysname 
)
AS
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;


--EXEC script_login @login_name = 'yanive'


CREATE TABLE #Data ([login] sysname, command nvarchar(4000));

DECLARE @name sysname, @type varchar (1), @hasaccess INT, @denylogin INT, @is_disabled INT, @PWD_varbinary  varbinary (256);
DECLARE @PWD_string  varchar (514), @SID_varbinary varbinary (85), @SID_string varchar (514), @command  varchar (1024);
DECLARE @is_policy_checked varchar (3),@is_expiration_checked varchar (3), @defaultdb sysname;
 

DECLARE login_curs CURSOR FOR
      SELECT p.sid, p.name, p.type, p.is_disabled, p.default_database_name, l.hasaccess, l.denylogin 
			FROM sys.server_principals p 
			LEFT JOIN sys.syslogins l ON ( l.name = p.name ) WHERE p.type IN ( 'S', 'G', 'U' ) AND p.name = @login_name;

OPEN login_curs;
FETCH NEXT FROM login_curs INTO @SID_varbinary, @name, @type, @is_disabled, @defaultdb, @hasaccess, @denylogin;

WHILE (@@fetch_status <> -1)
BEGIN;
  IF (@@fetch_status <> -2)
  BEGIN;
    IF (@type IN ( 'G', 'U'))
	-- NT authenticated account/group
    BEGIN;
			SELECT @command = 'CREATE LOGIN ' + QUOTENAME( @name ) + ' FROM WINDOWS WITH DEFAULT_DATABASE = [' + @defaultdb + ']';
    END;
    ELSE BEGIN; 
	-- SQL Server authentication
        -- obtain password and sid
        SELECT @PWD_varbinary = CAST( LOGINPROPERTY( @name, 'PasswordHash' ) AS varbinary (256) );
        EXEC sp_hexadecimal @PWD_varbinary, @PWD_string OUT;
        EXEC sp_hexadecimal @SID_varbinary,@SID_string OUT;
 
        -- obtain password policy state
        SELECT @is_policy_checked = CASE is_policy_checked WHEN 1 THEN 'ON' WHEN 0 THEN 'OFF' ELSE NULL END FROM sys.sql_logins WHERE name = @name;
        SELECT @is_expiration_checked = CASE is_expiration_checked WHEN 1 THEN 'ON' WHEN 0 THEN 'OFF' ELSE NULL END FROM sys.sql_logins WHERE name = @name;
 
        SELECT @command = 'CREATE LOGIN ' + QUOTENAME( @name ) + ' WITH PASSWORD = ' + @PWD_string + ' HASHED, SID = ' + @SID_string + ', DEFAULT_DATABASE = [' + @defaultdb + ']';

        IF ( @is_policy_checked IS NOT NULL )
        BEGIN;
          SELECT @command = @command + ', CHECK_POLICY = ' + @is_policy_checked;
        END
        IF ( @is_expiration_checked IS NOT NULL )
        BEGIN;
          SELECT @command = @command + ', CHECK_EXPIRATION = ' + @is_expiration_checked;
        END;
    END;
    IF (@denylogin = 1)
    BEGIN; -- login is denied access
      SET @command = @command + '; DENY CONNECT SQL TO ' + QUOTENAME( @name );
    END;    ELSE IF (@hasaccess = 0)
    BEGIN; -- login exists but does not have access
      SET @command = @command + '; REVOKE CONNECT SQL TO ' + QUOTENAME( @name );
    END;
    IF (@is_disabled = 1)
    BEGIN -- login is disabled
      SET @command = @command + '; ALTER LOGIN ' + QUOTENAME( @name ) + ' DISABLE';
    END;
		SELECT @command = @command + ';';
    INSERT #Data ([login], command ) SELECT @name, @command;
  END;

  FETCH NEXT FROM login_curs INTO @SID_varbinary, @name, @type, @is_disabled, @defaultdb, @hasaccess, @denylogin;
   END;
CLOSE login_curs; DEALLOCATE login_curs;


SELECT * FROM #Data;
RETURN 0;