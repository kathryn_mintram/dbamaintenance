﻿



-- =============================================
-- Author:		Shaun Salter-Baines
-- Create date: 16.11.2018
-- Description:	This SP finds Columns within Tables for PCI and GDPR
-- =============================================

CREATE PROCEDURE [sch_security].[SensitiveData] 


AS
BEGIN

--We want to make sure that when we collect the DBs we need to scan through, that we don't include this in our final results
SET NOCOUNT ON;


DECLARE @LoopCounter INT , 
		@MaxCounterId INT, 
        @DBName NVARCHAR(100), 
		@SQL NVARCHAR(MAX), 
		@SQL1 NVARCHAR(MAX),
		@ObjectName NVARCHAR(MAX)


IF OBJECT_ID('tempdb..#DBNames') IS NOT NULL DROP TABLE #DBNames
IF OBJECT_ID('tempdb..#PCITables') IS NOT NULL DROP TABLE #PCITables


--Clear out the results Table for new Data so that data is always relevant
TRUNCATE TABLE [sch_security].[GDPRPCITableCatalogue]
TRUNCATE TABLE [sch_security].[GDPRPCITablePrivileges]


--Store a list of DBs on the Instance so that we can loop through (we want Primary Dbs only)
SELECT
	[Name]
	 ,ROW_NUMBER() OVER(ORDER BY [Name] ASC) AS row_no
INTO #DBNames
FROM sys.dm_hadr_availability_replica_states ars
INNER JOIN sys.databases dbs
ON ars.replica_id = dbs.replica_id
WHERE role_desc = 'PRIMARY'
AND name NOT IN ('master', 'tempdb', 'model', 'msdb', 'DBAMaintenance')


SELECT @LoopCounter = min(row_no) , @MaxCounterId = max(row_no) 
FROM #DBNames

--==================================================================================================
--Populate PCI information first
--Get all Tables across all DBs that contain the columns 'Card'
--==================================================================================================
WHILE(@LoopCounter IS NOT NULL AND @LoopCounter <= @MaxCounterId)
BEGIN
   SELECT @DBName = [Name]
   FROM #DBNames WHERE row_no = @LoopCounter
   SET @SQL = 
			   'INSERT INTO [sch_security].[GDPRPCITableCatalogue] (DBName, SchemaName ,TableName, ColumnName, DataType, PCI_Or_GDPR)
			   SELECT' +
					QUOTENAME(@DBName, '''') + ' AS DBName,
					Schem.Name,
					obj.name,
					col.name,
					DataType.name,
					''PCI'' AS PCI
				FROM ' + QUOTENAME(@DBName) + '.SYS.columns col
				INNER JOIN ' + QUOTENAME(@DBName) + '.SYS.objects obj
					ON col.object_id = obj.object_id
				INNER JOIN ' + QUOTENAME(@DBName) + '.sys.types DataType
					ON Col.user_type_id = DataType.user_type_id
				INNER JOIN sys.schemas Schem
					ON Schem.schema_id = obj.schema_id
				WHERE col.name LIKE ''%Card%''
				AND obj.type  = ''U'''
   EXEC sp_executesql @SQL
   SET @LoopCounter  = @LoopCounter  + 1        
END

--Now we have our PCI data lets get the Tables and DBs and who has access to them
SELECT
  DISTINCT
  DBName
  ,TableName
  ,ROW_NUMBER() OVER(ORDER BY TableName ASC) AS row_no
INTO #PCITables 
FROM [sch_security].[GDPRPCITableCatalogue]

SELECT @LoopCounter = min(row_no) , @MaxCounterId = max(row_no) 
FROM #PCITables

WHILE(@LoopCounter IS NOT NULL AND @LoopCounter <= @MaxCounterId)
BEGIN
   SELECT @DBName = DBName,
		  @ObjectName = TableName
   FROM #PCITables WHERE row_no = @LoopCounter
   SET @SQL = 
				'INSERT INTO [sch_security].[GDPRPCITablePrivileges]
EXEC ' + QUOTENAME(@DBName) + '.[sch_security].[GDPRPCITablePrivileges] ''' + @ObjectName + ''''
   EXEC sp_executesql @SQL
   SET @LoopCounter  = @LoopCounter  + 1        
END


--Now we need to get rid of the DBO User who has access as we are only interested in 'external' parties
DELETE FROM [sch_security].[GDPRPCITablePrivileges]
WHERE UserWhoHasAccess NOT LIKE 'WIGGLEINTERNAL%' 


--==================================================================================================
--Now get all GDPR information
--==================================================================================================

--Reset the loop count
SELECT @LoopCounter = min(row_no) , @MaxCounterId = max(row_no) 
FROM #DBNames


WHILE(@LoopCounter IS NOT NULL AND @LoopCounter <= @MaxCounterId)
BEGIN
   SELECT @DBName = [Name]
   FROM #DBNames WHERE row_no = @LoopCounter
   SET @SQL = 
			   'INSERT INTO [sch_security].[GDPRPCITableCatalogue] (DBName, SchemaName ,TableName, ColumnName, DataType, PCI_Or_GDPR)
			   SELECT' +
					QUOTENAME(@DBName, '''') + ' AS DBName,
					Schem.Name,
					obj.name,
					col.name,
					DataType.name,
					''PCI'' AS PCI
				FROM ' + QUOTENAME(@DBName) + '.SYS.columns col
				INNER JOIN ' + QUOTENAME(@DBName) + '.SYS.objects obj
					ON col.object_id = obj.object_id
				INNER JOIN ' + QUOTENAME(@DBName) + '.sys.types DataType
					ON Col.user_type_id = DataType.user_type_id
				INNER JOIN sys.schemas Schem
					ON Schem.schema_id = obj.schema_id 
				WHERE (col.name LIKE ''%Address%''
				OR col.name LIKE ''%FirstName%''
				OR col.name LIKE ''%LastName%''
				OR col.name LIKE ''%FullName%''
				OR col.name LIKE ''%CustomerName%''
				OR col.name LIKE ''%Bank%''
				OR col.Name LIKE ''%PhoneNumber%''
				OR col.Name LIKE ''%City%''
				OR col.Name LIKE ''%Phone%'')
				AND obj.type  = ''U'''
   EXEC sp_executesql @SQL

   SET @LoopCounter  = @LoopCounter  + 1        
END

--Now we have our PCI data lets get the Tables and DBs and who has access to them
SELECT
  DISTINCT
  DBName
  ,TableName
  ,ROW_NUMBER() OVER(ORDER BY TableName ASC) AS row_no
INTO #GDPRTables 
FROM [sch_security].[GDPRPCITableCatalogue]

SELECT @LoopCounter = min(row_no) , @MaxCounterId = max(row_no) 
FROM #GDPRTables

WHILE(@LoopCounter IS NOT NULL AND @LoopCounter <= @MaxCounterId)
BEGIN
   SELECT @DBName = DBName,
		  @ObjectName = TableName
   FROM #GDPRTables WHERE row_no = @LoopCounter
   SET @SQL = 
				'INSERT INTO [sch_security].[GDPRPCITablePrivileges]
EXEC ' + QUOTENAME(@DBName) + '.[sch_security].[GDPRPCITablePrivileges] ''' + @ObjectName + ''''
   EXEC sp_executesql @SQL
   SET @LoopCounter  = @LoopCounter  + 1        
END


--Now we need to get rid of the DBO User who has access as we are only interested in 'external' parties
DELETE FROM [sch_security].[GDPRPCITablePrivileges]
WHERE UserWhoHasAccess NOT LIKE 'WIGGLEINTERNAL%' 


END


/****** Object:  StoredProcedure [sch_security].[SensitiveData_Access]    Script Date: 29/05/2019 14:59:18 ******/
SET ANSI_NULLS ON