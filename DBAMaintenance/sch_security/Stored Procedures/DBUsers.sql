﻿



------------------------------------------------------------------------------------------------------------------------------
-- [Security].[DBUsers] 
-- Purpose: Executes sp_who2 and stores the results for a single database
--
-- Date         Who							Version		Ticket			Comments			
-- 27/03/2018	Kathryn Mintram				1.00		DBACLN-7		Initial Write 
------------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [sch_security].[DBUsers] 
				@DBName NVARCHAR(32)

AS

IF OBJECT_ID('tempdb..#Who') IS NOT NULL DROP TABLE #Who

CREATE TABLE #Who
    (
      SPID INT,
      Status VARCHAR(1000) NULL,
      Login SYSNAME NULL,
      HostName SYSNAME NULL,
      BlkBy SYSNAME NULL,
      DBName SYSNAME NULL,
      Command VARCHAR(1000) NULL,
      CPUTime INT NULL,
      DiskIO INT NULL,
      LastBatch VARCHAR(1000) NULL,
      ProgramName VARCHAR(1000) NULL,
      SPID2 INT, 
	  REQUESTID INT NULL --comment out for SQL 2000 databases
    )


INSERT  INTO #Who
EXEC sp_who2

IF (NOT EXISTS(SELECT Login FROM [DBAMaintenance].[sch_security].[DBUserList]))
BEGIN
	INSERT INTO [DBAMaintenance].[sch_security].[DBUserList] (Login, Hostname, DBName, ProgramName)
	SELECT Login, HostName, DBName, ProgramName
	FROM #Who
	WHERE DBName = 'Reports'
	AND DBName IS NOT NULL
END

DROP TABLE #Who


/****** Object:  StoredProcedure [sch_security].[GroupMembers]    Script Date: 29/05/2019 14:57:15 ******/
SET ANSI_NULLS ON