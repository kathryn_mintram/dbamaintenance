﻿


--|=====================================================================================================================
--|  Name:				|	EmailInactiveADUserList
--|  Purpose:			|	Emails out any AD users added to our Windows groups that are no longer active
--|--------------------------------------------------------------------------------------------------------------------
--|  Date 				| Who				        | Version			    	|  Comments
--|	26.07.2019			| Kathryn Mintram			| 1.00						|  Initital write
--|======================================================================================================================

CREATE PROCEDURE [sch_security].[EmailInactiveADUserList]

AS

--Email out from SQL Server

DECLARE 
@RecipientsList NVARCHAR(MAX),
@xml NVARCHAR(MAX),
@body NVARCHAR(MAX),
--@body2 NVARCHAR(MAX),
--@body3 NVARCHAR(MAX),
--@body4 NVARCHAR(MAX),
@EmailContent NVARCHAR(MAX)


IF OBJECT_ID('tempdb..#SpaceSaved') IS NOT NULL DROP TABLE #SpaceSaved
IF OBJECT_ID('tempdb..#TimeTaken') IS NOT NULL DROP TABLE #TimeTaken;


SET @RecipientsList = 'teri.borland@wiggle.com;shaun.baines@wiggle.com;kathryn.mintram@wiggle.com'


SET @xml = CAST(( 
SELECT 
	[Name] AS 'td'
	,''
	,[Enabled] AS 'td'
	,''
	,LastLogonDate AS 'td'
	,''
	,AccountExpirationDate AS 'td'
	,''
FROM sch_security.ADUsers
WHERE AccountExpirationDate IS NOT NULL
FOR XML PATH('tr'), ELEMENTS ) AS NVARCHAR(MAX))


SET @body ='<html><body>
<H2> </H2>
<p>
<p>List of inactive AD users</p>
<table border = 1> 
<tr>
<th> Name </th>
<th> Enabled </th>
<th> Last LogonDate </th>
<th> Account Expiration Date </th>
</tr>'    

IF @body IS NOT NULL
	BEGIN
		SET @body = @body + @xml +'</table></body></html>'
		SET @EmailContent = @body
	END


--=============================================
-- Send out the email
--=============================================
EXEC msdb.dbo.sp_send_dbmail
@profile_name = 'SQL Support',
@body = @EmailContent,
@body_format ='HTML',
@recipients = @RecipientsList,
@subject = 'Inactive AD Users';