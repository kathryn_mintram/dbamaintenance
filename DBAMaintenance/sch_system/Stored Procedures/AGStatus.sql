﻿

--=======================================================================================================

-- Name/Description: sGetSystem_AGStates - This gets the same info the Availability Group Dashboard shows
-- Author: Shaun Salter-Baines/Mark Payne
-- Date: 02.08.2018
--=======================================================================================================

CREATE PROCEDURE [sch_system].[AGStatus] as


SET NOCOUNT ON


-- AG Status 
DECLARE @HADRName    varchar(25) 
SET @HADRName = @@SERVERNAME 
SELECT n.group_name
		,n.replica_server_name
		,n.node_name,rs.role_desc, 
db_name(drs.database_id) as 'DBName',drs.synchronization_state_desc,drs.synchronization_health_desc 
FROM sys.dm_hadr_availability_replica_cluster_nodes n 
JOIN sys.dm_hadr_availability_replica_cluster_states cs 
ON n.replica_server_name = cs.replica_server_name 
JOIN sys.dm_hadr_availability_replica_states rs  
ON rs.replica_id = cs.replica_id 
JOIN sys.dm_hadr_database_replica_states drs 
ON rs.replica_id=drs.replica_id 
WHERE n.replica_server_name <> @HADRName