﻿

--=======================================================================================================

-- Name/Description: sGetSystem_GetAGLatency - Gets the latency between the Primary and Secondary nodes
-- Author: Shaun Salter-Baines - inspired by the script on mssqltips.com
-- Date: 02.08.2018
--=======================================================================================================

CREATE PROCEDURE [sch_system].[AGLatency] as


SET NOCOUNT ON


SELECT 
	ag.[name] as 'Distributed AG Name', 
	ar.replica_server_name as 'Underlying AG', 
	dbs.[name] as 'DB', 
	drs.synchronization_health_desc as 'Sync Status', 
	drs.log_send_queue_size, 
	drs.log_send_rate, 
	drs.redo_queue_size, 
	drs.redo_rate, 
	drs.last_commit_time,
	drs.secondary_lag_seconds,
	(CONVERT(VARCHAR(12), drs.secondary_lag_seconds / 60 / 60 % 24) 
	+ ':' + CONVERT(VARCHAR(2), drs.secondary_lag_seconds / 60 % 60) 
	+ ':' + CONVERT(VARCHAR(2), drs.secondary_lag_seconds % 60)) AS RPO, 
	((drs.secondary_lag_seconds/60)+1) AS RPOmins
FROM sys.databases dbs
  INNER JOIN sys.dm_hadr_database_replica_states drs
    ON dbs.database_id = drs.database_id
  INNER JOIN sys.availability_groups ag
    ON drs.group_id = ag.group_id
  INNER JOIN sys.dm_hadr_availability_replica_states ars
    ON ars.replica_id = drs.replica_id
  INNER JOIN sys.availability_replicas ar
    ON ar.replica_id = ars.replica_id
WHERE ag.is_distributed = 1
AND secondary_lag_seconds > 0

ORDER BY secondary_lag_seconds DESC;