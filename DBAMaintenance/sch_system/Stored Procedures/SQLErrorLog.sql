﻿

--=======================================================================================================
-- Name/Description: sGetSystem_ErrorLog - Checks the error Log for errors
-- Author: Shaun Salter-Baines - inspired by the script on mssqltips.com
-- Date: 02.08.2018
--=======================================================================================================

CREATE PROCEDURE [sch_system].[SQLErrorLog] as


SET NOCOUNT ON

DECLARE @Time_Start datetime;
DECLARE @Time_End datetime;
SET @Time_Start=getdate()-2;
SET @Time_End=getdate();

CREATE TABLE #ErrorLog (logdate datetime
                      , processinfo varchar(255)
                      , Message varchar(500) )

INSERT #ErrorLog (logdate, processinfo, Message)
   EXEC master.dbo.xp_readerrorlog 0, 1, null, null , @Time_Start, @Time_End, N'desc';

--CREATE TABLE SQL_Log_Errors (
--	[logdate] datetime,
--    [Message] varchar (500) )

--insert into [System].[SQLLogErrors]
SELECT 
	LogDate, 
	Message 
FROM #ErrorLog
WHERE (Message LIKE '%error%' OR Message LIKE '%failed%') 
AND processinfo NOT LIKE 'logon'
ORDER BY logdate desc


DROP TABLE #ErrorLog