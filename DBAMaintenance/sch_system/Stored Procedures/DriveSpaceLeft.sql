﻿
--=======================================================================================================

-- Name/Description: sGetSystem_DriveSpaceLeft - gets the drive space left 
-- Author: Shaun Salter-Baines - inspired by the script on mssqltips.com
-- Date: 02.08.2018
--=======================================================================================================

CREATE PROCEDURE [sch_system].[DriveSpaceLeft] as


SET NOCOUNT ON


CREATE TABLE #DriveSpaceLeft (Drive varchar(10),
                              [MB Free] bigint )

INSERT #DriveSpaceLeft (Drive, [MB Free])
   EXEC master.dbo.xp_fixeddrives;

INSERT INTO [sch_system].[DrivesSpaceIssues] 
  select Drive, [MB Free] from #DriveSpaceLeft
  where [MB Free] < 1000

DROP TABLE #DriveSpaceLeft



SELECT DISTINCT DB_NAME(dovs.database_id) DBName,
dovs.logical_volume_name AS LogicalName,
dovs.volume_mount_point AS Drive,
CONVERT(INT,dovs.available_bytes/1048576.0) AS FreeSpaceInMB
FROM sys.master_files mf
CROSS APPLY sys.dm_os_volume_stats(mf.database_id, mf.FILE_ID) dovs
ORDER BY FreeSpaceInMB ASC


SELECT
 SUBSTRING(a.FILENAME, 1, 1) Drive,
 [FILE_SIZE_MB] = convert(decimal(12,2),
round(a.size/128.000,2)),
 [SPACE_USED_MB] = convert(decimal(12,2),
round(fileproperty(a.name,'SpaceUsed')/128.000,2)),
 [FREE_SPACE_MB] = convert(decimal(12,2),
round((a.size-fileproperty(a.name,'SpaceUsed'))/128.000,2)) ,
 [FREE_SPACE_%] = convert(decimal(12,2),
(convert(decimal(12,2),round((a.size-fileproperty(a.name,'SpaceUsed'))/128.000,2)) 
/ convert(decimal(12,2),round(a.size/128.000,2)) * 100)),
 a.NAME, a.FILENAME
FROM dbo.sysfiles a
ORDER BY Drive, [Name]