﻿CREATE procedure [sch_system].[AGStats]
as 
begin

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE name = 'AGStatistics' AND type='U') 
BEGIN 
	CREATE TABLE [sch_system].[AGStatistics] 
	( 
		Distributed_AG sysname NULL, 
		AG nvarchar(256) NOT NULL, 
		DatabaseName sysname NULL, 
		role_desc nvarchar(60) NULL, 
		synchronization_health_desc nvarchar(60) NULL, 
		log_send_queue_size bigint NULL, 
		log_send_rate bigint NULL, 
		redo_queue_size  bigint, 
		redo_rate  bigint,
		suspend_reason_desc nvarchar(60),
		last_sent_time datetime,
		last_received_time datetime,
		last_hardened_time datetime,
		last_redone_time datetime,
		last_commit_time datetime,
		secondary_lag_seconds bigint,
		log_reuse_wait_desc nvarchar(60), 
		Report_Date datetime default getdate() 
	)
	--ON PRIMARY 
END 

INSERT 	[sch_system].[AGStatistics] (Distributed_AG , 
		AG , 
		DatabaseName , 
		role_desc , 
		synchronization_health_desc, 
		log_send_queue_size , 
		log_send_rate, 
		redo_queue_size  , 
		redo_rate  ,
		suspend_reason_desc,
		last_sent_time ,
		last_received_time ,
		last_hardened_time ,
		last_redone_time ,
		last_commit_time ,
		secondary_lag_seconds ,
		log_reuse_wait_desc  )
SELECT ag.name as 'Distributed_AG', 
	ar.replica_server_name as 'AG', 
	dbs.name as 'DatabaseName', 
	ars.role_desc, 
	drs.synchronization_health_desc, 
	drs.log_send_queue_size, 
	drs.log_send_rate, 
	drs.redo_queue_size, 
	drs.redo_rate,
	drs.suspend_reason_desc,
	drs.last_sent_time,
	drs.last_received_time,
	drs.last_hardened_time,
	drs.last_redone_time,
	drs.last_commit_time,
	drs.secondary_lag_seconds,
	dbs.log_reuse_wait_desc
FROM sys.databases dbs 
    INNER JOIN sys.dm_hadr_database_replica_states drs on dbs.database_id = drs.database_id
	INNER JOIN sys.availability_groups ag ON drs.group_id = ag.group_id
	INNER JOIN sys.dm_hadr_availability_replica_states ars ON ars.replica_id = drs.replica_id
    INNER JOIN sys.availability_replicas ar ON ar.replica_id = ars.replica_id
END