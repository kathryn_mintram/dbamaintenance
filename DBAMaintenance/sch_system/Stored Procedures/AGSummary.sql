﻿

--=======================================================================================================

-- Name/Description: sGetSystem_AGStates - This gets the same info the Availability Group Dashboard shows
-- Author: Shaun Salter-Baines/Mark Payne
-- Date: 02.08.2018
--=======================================================================================================

CREATE PROCEDURE [sch_system].[AGSummary] as


SET NOCOUNT ON


-- List AG Replica Details 
SELECT n.group_name
		,n.replica_server_name
		,n.node_name,rs.role_desc 
FROM sys.dm_hadr_availability_replica_cluster_nodes n 
JOIN sys.dm_hadr_availability_replica_cluster_states cs 
	ON n.replica_server_name = cs.replica_server_name 
JOIN sys.dm_hadr_availability_replica_states rs  
	ON rs.replica_id = cs.replica_id 
 

SET ANSI_NULLS ON