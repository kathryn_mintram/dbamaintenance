﻿CREATE TABLE [sch_system].[AGStatistics] (
    [Distributed_AG]              [sysname]      NULL,
    [AG]                          NVARCHAR (256) NOT NULL,
    [DatabaseName]                [sysname]      NULL,
    [role_desc]                   NVARCHAR (60)  NULL,
    [synchronization_health_desc] NVARCHAR (60)  NULL,
    [log_send_queue_size]         BIGINT         NULL,
    [log_send_rate]               BIGINT         NULL,
    [redo_queue_size]             BIGINT         NULL,
    [redo_rate]                   BIGINT         NULL,
    [suspend_reason_desc]         NVARCHAR (60)  NULL,
    [last_sent_time]              DATETIME       NULL,
    [last_received_time]          DATETIME       NULL,
    [last_hardened_time]          DATETIME       NULL,
    [last_redone_time]            DATETIME       NULL,
    [last_commit_time]            DATETIME       NULL,
    [secondary_lag_seconds]       BIGINT         NULL,
    [log_reuse_wait_desc]         NVARCHAR (60)  NULL,
    [Report_Date]                 DATETIME       DEFAULT (getdate()) NULL
);

