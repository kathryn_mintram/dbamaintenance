﻿---- =============================================
---- Author: Shaun Salter-Baines
---- Create date: 19.12.2017
---- Description: Memory.FileGrowth
---- =============================================

CREATE PROCEDURE [sch_memory].[FileGrowth] 

AS
BEGIN


SET NOCOUNT OFF


DECLARE @filename NVARCHAR(1000);
DECLARE @bc INT;
DECLARE @ec INT;
DECLARE @bfn VARCHAR(1000);
DECLARE @efn VARCHAR(10);
 
--Our Temp objects should be out of memory, but just in case they haven't been removed, drop them
IF OBJECT_ID('tempdb..#DBGrowth') IS NOT NULL DROP TABLE #DBGrowth
IF OBJECT_ID('tempdb..#Memory') IS NOT NULL DROP TABLE #Memory
IF OBJECT_ID('tempdb..#DBSizes') IS NOT NULL DROP TABLE #DBSizes


-- This part has been borrowed from RedGate with some amendments 
-- Get the name of the current default trace
SELECT @filename = CAST(value AS NVARCHAR(1000))
FROM ::fn_trace_getinfo(DEFAULT)
WHERE traceid = 1 AND property = 2;
 
-- rip apart file name into pieces
SET @filename = REVERSE(@filename);
SET @bc = CHARINDEX('.',@filename);
SET @ec = CHARINDEX('_',@filename)+1;
SET @efn = REVERSE(SUBSTRING(@filename,1,@bc));
SET @bfn = REVERSE(SUBSTRING(@filename,@ec,LEN(@filename)));
 
-- set filename without rollover number
SET @filename = @bfn + @efn
 
-- process all trace files
SELECT 
  CONVERT(DATE, ftg.StartTime) AS StartTime
  ,te.name AS EventName
  ,DB_NAME(ftg.databaseid) AS DatabaseName
  ,ftg.databaseid
  ,ftg.[Filename]
  ,(ftg.IntegerData*8)/1024.0 AS GrowthMB 
  ,(ftg.duration/1000)AS DurMS
INTO #DBGrowth
FROM ::fn_trace_gettable(@filename, DEFAULT) AS ftg 
INNER JOIN sys.trace_events AS te 
	ON ftg.EventClass = te.trace_event_id  
WHERE (ftg.EventClass = 92  -- Date File Auto-grow
    OR ftg.EventClass = 93) -- Log File Auto-grow
ORDER BY ftg.StartTime DESC


-- This part was borrowed from Brent Ozar with adjustments
-- Get memory information
SELECT 
	CAST(COUNT(*) * 8 / 1024.0 AS NUMERIC(10, 2)) AS CachedDataMB,
	CASE MEM.database_id WHEN 32767 THEN 'ResourceDb' ELSE DB_NAME(MEM.database_id) END AS DatabaseName,
	MEM.database_id
INTO #Memory
FROM sys.dm_os_buffer_descriptors MEM
GROUP BY DB_NAME(MEM.database_id) , MEM.database_id 
ORDER BY 1 DESC


--Get DB size information
SELECT 
	DB.database_id
	,MF.Name AS Logicalname
	,Db.Name AS DBNAME
	,CASE WHEN MF.type_desc = 'ROWS'
		THEN 'DBFile'
	WHEN MF.type_desc = 'LOG'
		THEN 'LogFile'
	ELSE MF.type_desc
	END AS FileType
	,MF.Physical_name As FileLoc
	,MF.State_Desc
	,SUM(MF.size * 8 / 1024.0 /1024.0) AS DataFileSizeGB
	---1 means that the file will grow until the disk is full
	--268435456 means Log file will grow to a maximum size of 2 TB.
INTO #DBSizes
FROM
    sys.master_files MF
    JOIN sys.databases DB 
		ON DB.database_id = MF.database_id
WHERE DB.source_database_id is null -- exclude snapshots
GROUP BY DB.database_id, MF.Name, DB.name, MF.type_desc, MF.Physical_name, MF.State_Desc, MF.max_size


--Get Growth Sizes
-- Note that the current DB file size will be static when this is first run, however as time goes on, we should see it grow
--INSERT INTO [DBAMaintenance].[dbo].[DBFile_GrowthSize]
--	([StartTime]
--	,[EventName]
--	,[DatabaseName]
--	,[FileName]
--	,[GrowthMB]
--	,[DataFileSizeGB])
SELECT
	#DBGrowth.StartTime
	,#DBGrowth.EventName
	,#DBGrowth.DatabaseName
	,#DBGrowth.[Filename]
	,CAST(SUM(#DBGrowth.GrowthMB) AS NUMERIC(10,2)) AS GrowthMB
	,CAST(#DBSizes.DataFileSizeGB AS NUMERIC(10, 2)) As DataFileSizeGB
FROM #DBGrowth
INNER JOIN #DBSizes
	ON #DBSizes.database_id = #DBGrowth.DatabaseID
	AND #DBSizes.Logicalname = #DBGrowth.FileName
WHERE StartTime = CONVERT(DATE, GETDATE()) --The database file size will change so therefore we must pull back todays date only
AND NOT EXISTS (SELECT
				  DBG.[StartTime]
				  ,DBG.[EventName]
				  ,DBG.[DatabaseName]
				  ,DBG.[FileName]
				  ,DBG.[GrowthMB]
				  ,DBG.[DataFileSizeGB]
				FROM [DBAMaintenance].[sch_memory].[GrowthSize] DBG
				WHERE DBG.[StartTime] = #DBGrowth.[StartTime]
				AND DBG.[EventName] = #DBGrowth.[EventName]
				AND DBG.[DatabaseName] = #DBGrowth.[DatabaseName]
				AND DBG.[FileName] = #DBGrowth.[FileName]
				AND DBG.[GrowthMB] = #DBGrowth.[GrowthMB]
				AND DBG.[DataFileSizeGB] = #DBSizes.[DataFileSizeGB]
				) 
GROUP BY StartTime, EventName, DatabaseName, [FileName],DataFileSizeGB
ORDER BY DatabaseName, StartTime DESC


UPDATE DBFileLoc
SET DBFileLoc.[LogicalName]= DB.LogicalName
	,DBFileLoc.[DBName] = DB.DBNAME
	,DBFileLoc.[FileType] = DB.FileType
	,DBFileLoc.[FileLoc] = DB.FileLoc
	,DBFileLoc.[State_Desc] = DB.state_desc
	,DBFileLoc.[DateFileSizeGB] = CAST(DB.DataFileSizeGB AS NUMERIC(10, 2))
	,DBFileLoc.[CachedDataMB] = MEM.CachedDataMB
FROM [sch_memory].[LocSizeCacheSize] DBFileLoc
INNER JOIN #DBSizes DB
	ON DB.LogicalName = DBFileLoc.LogicalName
INNER JOIN #Memory MEM
	ON DB.database_id = MEM.database_id

--Will need to replace with a merge

	
END


/****** Object:  StoredProcedure [sch_memory].[PlanCacheStats]    Script Date: 29/05/2019 14:44:42 ******/
SET ANSI_NULLS ON