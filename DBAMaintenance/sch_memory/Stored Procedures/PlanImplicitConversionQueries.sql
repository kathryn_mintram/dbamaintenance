﻿


-- =============================================
-- Author:		Shaun Salter-Baines
-- Create date: 13.11.2017
-- Description:	This SP finds all queries that have implicit conversion issues
-- =============================================

CREATE PROCEDURE [sch_memory].[PlanImplicitConversionQueries] 
	

AS
BEGIN

--We want to make sure that when we collect the DBs we need to scan through, that we don't include this in our final results
SET NOCOUNT ON;


DECLARE @LoopCounter INT , 
		@MaxCounterId INT, 
        @DBName NVARCHAR(100), 
		@SQL NVARCHAR(MAX)


IF OBJECT_ID('tempdb..#DBNames') IS NOT NULL DROP TABLE #DBNames

TRUNCATE TABLE [sch_memory].[ImplicitQueryConversionIssues]

--Store a list of DBs on the Instance so that we can loop through
SELECT
	[Name]
	 ,ROW_NUMBER() OVER(ORDER BY [Name] ASC) AS row_no
INTO #DBNames
FROM sys.databases
--Make sure that System DBs and anything in a restore state is not included.
WHERE state_desc = 'ONLINE'
AND name NOT IN ('master', 'tempdb', 'model', 'msdb', 'DBAMaintenance')

SET NOCOUNT OFF;


SELECT @LoopCounter = min(row_no) , @MaxCounterId = max(row_no) 
FROM #DBNames

 
WHILE(@LoopCounter IS NOT NULL AND @LoopCounter <= @MaxCounterId)
BEGIN
   SELECT @DBName = [Name]
   FROM #DBNames WHERE row_no = @LoopCounter

SET @SQL= '
	USE ' + QUOTENAME(@DBName) +
	'  
	INSERT INTO [DBAMaintenance].[sch_memory].[ImplicitQueryConversionIssues] 
			   ([Database Name]
			   ,[Query Text]
			   ,[Total Worker Time]
			   ,[Avg Worker Time]
			   ,[Max Worker Time]
			   ,[Avg Elapsed Time]
			   ,[Max Elapsed Time]
			   ,[Avg Logical Reads]
			   ,[Max Logical Reads]
			   ,[Execution Count]
			   ,[Creation Time]
			   ,[Query Plan]) 
	SELECT
	DB_NAME(t.[dbid]) AS [Database Name], 
	t.text AS [Query Text],
	qs.total_worker_time AS [Total Worker Time], 
	qs.total_worker_time/qs.execution_count AS [Avg Worker Time], 
	qs.max_worker_time AS [Max Worker Time], 
	qs.total_elapsed_time/qs.execution_count AS [Avg Elapsed Time], 
	qs.max_elapsed_time AS [Max Elapsed Time],
	qs.total_logical_reads/qs.execution_count AS [Avg Logical Reads],
	qs.max_logical_reads AS [Max Logical Reads], 
	qs.execution_count AS [Execution Count], 
	qs.creation_time AS [Creation Time],
	qp.query_plan AS [Query Plan]
	FROM sys.dm_exec_query_stats AS qs WITH (NOLOCK)
	CROSS APPLY sys.dm_exec_sql_text(plan_handle) AS t 
	CROSS APPLY sys.dm_exec_query_plan(plan_handle) AS qp 
	WHERE CAST(query_plan AS NVARCHAR(MAX)) LIKE (''%CONVERT_IMPLICIT%'')
	 AND t.[dbid] = DB_ID()
	ORDER BY qs.total_worker_time DESC OPTION (RECOMPILE);'

	EXEC sp_executesql @SQL

   SET @LoopCounter  = @LoopCounter  + 1        
END


END


/****** Object:  StoredProcedure [sch_memory].[QueryUsage]    Script Date: 29/05/2019 14:46:01 ******/
SET ANSI_NULLS ON