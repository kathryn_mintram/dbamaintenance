﻿
-- =============================================
-- Author:		Shaun Salter-Baines
-- Create date: 29.11.2018
-- Description:	Get memory Status for Instance
-- =============================================
CREATE PROCEDURE [sch_memory].[Status] 

AS
BEGIN


DBCC MEMORYSTATUS;


/*check for internal memory pressure leading to evictions - 
non-zero counters here would indicate that something not good is going on with the plan cache:*/
SELECT * FROM sys.dm_os_memory_cache_clock_hands 
  WHERE [type] IN (N'CACHESTORE_SQLCP', N'CACHESTORE_OBJCP');


END