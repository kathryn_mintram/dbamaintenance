﻿-- =============================================
-- Author:		Kathryn Mintram
-- Create date: 12.12.2018
-- Description:	Gather information from the buffer cache, including space used & free, clean/dirty pages etc
-- =============================================
CREATE PROCEDURE [sch_memory].[BufferCache] 

AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO  [sch_memory].[BufferCacheDetails]
	(IndexName, ObjectName, ObjectType, BufferPages, BufferTotalMB, FreeSpaceMB, 
	BufferFreeSpacePct, DirtyPages, CleanPages, DirtyPagesMB, CleanPagesMB)
	SELECT indexes.name AS IndexName
		,objects.name AS ObjectName
		,objects.type_desc AS ObjectType
		,COUNT(*) AS BufferPages
		,CAST(COUNT(*) * 8 AS DECIMAL) / 1024 AS BufferTotalMB
		,CAST(SUM(CAST(dm_os_buffer_descriptors.free_space_in_bytes AS BIGINT)) AS DECIMAL) / 1024 / 1024 AS FreeSpaceMB
		,CAST((CAST(SUM(CAST(dm_os_buffer_descriptors.free_space_in_bytes AS BIGINT)) AS DECIMAL) / 1024 / 1024) / (CAST(COUNT(*) * 8 AS DECIMAL) / 1024) * 100 AS DECIMAL(5, 2)) AS FreeSpacePct
		,SUM(CASE 
				WHEN dm_os_buffer_descriptors.is_modified = 1
					THEN 1
					ELSE 0
				END) AS DirtyPages
		,SUM(CASE 
				WHEN dm_os_buffer_descriptors.is_modified = 1
					THEN 0
					ELSE 1
				END) AS CleanPages
		,SUM(CASE 
				WHEN dm_os_buffer_descriptors.is_modified = 1
					THEN 1
					ELSE 0
				END) * 8 / 1024 AS DirtyPagesMB
		,SUM(CASE 
				WHEN dm_os_buffer_descriptors.is_modified = 1
					THEN 0
					ELSE 1
				END) * 8 / 1024 AS CleanPagesMB
	FROM sys.dm_os_buffer_descriptors
	INNER JOIN sys.allocation_units ON allocation_units.allocation_unit_id = dm_os_buffer_descriptors.allocation_unit_id
	INNER JOIN sys.partitions ON (
			(
				allocation_units.container_id = partitions.hobt_id
				AND type IN (1,3)
			)
			OR 
			(
				allocation_units.container_id = partitions.partition_id
				AND type IN (2))
			)
	INNER JOIN sys.objects ON partitions.object_id = objects.object_id
	INNER JOIN sys.indexes ON objects.object_id = indexes.object_id
		AND partitions.index_id = indexes.index_id
	WHERE allocation_units.type IN (1,2,3)
		AND objects.is_ms_shipped = 0
		AND dm_os_buffer_descriptors.database_id = DB_ID()
	GROUP BY indexes.name
		,objects.name
		,objects.type_desc
	ORDER BY COUNT(*) DESC;


END


/****** Object:  StoredProcedure [sch_memory].[FileGrowth]    Script Date: 29/05/2019 14:43:11 ******/
SET ANSI_NULLS ON