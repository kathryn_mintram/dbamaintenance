﻿


-- =============================================
-- Author:		Shaun Salter-Baines
-- Create date: 29.11.2018
-- Description:	see how the plan cache is being used with the following

-- NOTE  - Needs multiple DB functionality
-- =============================================
CREATE PROCEDURE [sch_memory].[PlanCacheSize] 
@DBName VARCHAR(100)

AS
BEGIN

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'
USE ' + @DBName + 
' SELECT objtype, cacheobjtype,
    AVG(size_in_bytes*1.0)/1024.0/1024.0,
    MAX(size_in_bytes)/1024.0/1024.0,
    SUM(size_in_bytes)/1024.0/1024.0,
    COUNT(*)
FROM sys.dm_exec_cached_plans
GROUP BY GROUPING SETS ((),(objtype, cacheobjtype))
ORDER BY objtype, cacheobjtype;'

EXEC sp_executesql @SQL


END