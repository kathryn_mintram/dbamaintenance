﻿

 --=============================================
 --Author:		Shaun Salter-Baines
 --Create date: 20.08.2018
 --Description:	Gets the plan cache for a specific DB and records the % of plans that are compiled vs adhoc, single use vs multiple use
 --NOTE  - Needs multiple DB functionality
 --=============================================
CREATE PROCEDURE [sch_memory].[PlanCacheUsage_Needs working] 
@DBName VARCHAR(100)

AS

BEGIN


DECLARE
--@Table TABLE (sumOfCacheEntries FLOAT),
@SQL NVARCHAR(MAX),
@SQL2 NVARCHAR(MAX),
@sumOfCacheEntries FLOAT,
@singleUse FLOAT, 
@multiUse FLOAT, 
@total FLOAT



SET @SQL = '( SELECT COUNT(*) FROM ' + QUOTENAME(@DBName) + '.sys.dm_exec_cached_plans )'

exec sp_executesql @SQL
--SET @sumOfCacheEntries

--print @sumOfCacheEntries

SET @SQL = 
'SELECT  objtype, 
   ROUND((CAST(COUNT(*) AS FLOAT) / ' + @sumOfCacheEntries + ') * 100,2) [pc_In_Cache]
FROM  ' + QUOTENAME(@DBName) + '.sys.dm_exec_cached_plans p 
GROUP BY objtype 
ORDER BY 2'

exec sp_executesql @SQL


SET @singleUse = '( SELECT COUNT(*) 
     FROM ' + QUOTENAME(@DBName) + '.sys.dm_exec_cached_plans 
     WHERE cacheobjtype = ''Compiled Plan''
     AND usecounts = 1)'
exec sp_executesql @singleUse

SET @multiUse =  '( SELECT COUNT(*) 
     FROM ' + QUOTENAME(@DBName) + '.sys.dm_exec_cached_plans 
     WHERE cacheobjtype = ''Compiled Plan'' 
     AND usecounts > 1)'
exec sp_executesql @multiUse

SET @total = @singleUse + @multiUse


SELECT 'Single Usecount', ROUND((@singleUse / @total) * 100,2) [pc_single_usecount]
UNION ALL
SELECT 'Multiple Usecount', ROUND((@multiUse / @total) * 100,2) 

SET @SQL2 = 
'SELECT TOP 100 t.[text], plan_handle
FROM ' + QUOTENAME(@DBName) + '.sys.dm_exec_cached_plans p
CROSS APPLY ' + QUOTENAME(@DBName) + '.sys.dm_exec_sql_text(p.plan_handle) t
WHERE usecounts = 1
AND  cacheobjtype = ''Compiled Plan''
AND  objtype = ''Adhoc''
AND  t.[text] NOT LIKE ''%SELECT%TOP%10%t%text%'''

exec sp_executesql @SQL2


END