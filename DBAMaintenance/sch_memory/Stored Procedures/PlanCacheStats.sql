﻿-- =============================================
-- Author:		Kathryn Mintram
-- Create date: 12.12.2018
-- Description:	Gather information from the plan cache, such as usecount, refcount, created_date, last_executed,
-- DatabaseName, query text etc.... 
-- =============================================
CREATE PROCEDURE [sch_memory].[PlanCacheStats] 

AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [sch_memory].[PlanCacheDetails]
	(UseCounts,RefCounts, CacheObjType, ObjType, SizeInBytes, 
	DatabaseName, CreatedDate, LastExecuted, Query)
	   SELECT cplan.usecounts
			,cplan.refcounts
			,cplan.cacheobjtype
			,cplan.objtype
			,cplan.size_in_bytes
			,ISNULL(DB_NAME(qtext.dbid),'ResourceDB') AS DatabaseName
			,qstat.creation_time
			,qstat.last_execution_time
			,qtext.TEXT
		FROM sys.dm_exec_cached_plans AS cplan
		JOIN sys.dm_exec_query_stats AS qstat
			ON cplan.plan_handle = qstat.plan_handle
		CROSS APPLY sys.dm_exec_sql_text(cplan.plan_handle) AS qtext
		CROSS APPLY sys.dm_exec_query_plan(cplan.plan_handle) AS qplan
		ORDER BY cplan.usecounts DESC

END


/****** Object:  StoredProcedure [sch_memory].[PlanImplicitConversionQueries]    Script Date: 29/05/2019 14:45:10 ******/
SET ANSI_NULLS ON