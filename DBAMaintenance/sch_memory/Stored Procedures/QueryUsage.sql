﻿
-- =============================================
-- Author:		Kathryn Mintram
-- Create date: 18-12-2018
-- Description:	Gets memory usage for queries which queries have high memory usage
-- =============================================
CREATE PROCEDURE [sch_memory].[QueryUsage]

AS
BEGIN

	SET NOCOUNT ON;

DECLARE @LoopCounter INT , 
		@MaxCounterId INT, 
		@DBName NVARCHAR(100), 
		@SQL NVARCHAR(MAX)

IF OBJECT_ID('tempdb..#DBNames') IS NOT NULL DROP TABLE #DBNames

--Store a list of DBs on the Instance so that we can loop through
SELECT
	[Name]
	 ,ROW_NUMBER() OVER(ORDER BY [Name] ASC) AS row_no
INTO #DBNames
FROM sys.databases
--Make sure that System DBs and anything in a restore state is not included.
WHERE state_desc = 'ONLINE'
AND name NOT IN ('master', 'tempdb', 'model', 'msdb')

CREATE TABLE #Memory (
	DBName NVARCHAR(100)
	,Request_Time datetime
	,Grant_Time datetime
	,Requested_Memory_KB int
	,Granted_Memory_KB int
	,Required_Memory_KB int
	,Used_Memory_KB int
	,Max_Used_Memory_KB int
	,Wait_Time_MS int
	,Query_Text NVARCHAR(MAX)
	)

--Setup a loop counter 
SELECT @LoopCounter = min(row_no) , @MaxCounterId = max(row_no) 
FROM #DBNames
 
 --Setup where conditions for loop
WHILE(@LoopCounter IS NOT NULL AND @LoopCounter <= @MaxCounterId)

--Complete steps below until until condition is no longer true
BEGIN
   SELECT @DBName = [Name]
   FROM #DBNames WHERE row_no = @LoopCounter
	   SET @SQL = 'INSERT INTO [sch_memory].[MemoryHeavyQueries] (DBName, Request_Time, Grant_Time, Requested_Memory_KB, Granted_Memory_KB, Required_Memory_KB, 
										Used_Memory_KB, Max_Used_Memory_KB, Wait_Time_MS, Query_Text)
					SELECT ' +
						QUOTENAME(@DBName, '''') + ' AS DBName
						,mg.request_time
						,mg.grant_time
						,mg.requested_memory_kb
						,mg.granted_memory_kb
						,mg.required_memory_kb
						,mg.used_memory_kb
						,mg.max_used_memory_kb
						,mg.wait_time_ms
						,pt.text
					FROM ' + QUOTENAME(@DBName) + '.sys.dm_exec_query_memory_grants mg
					JOIN ' + QUOTENAME(@DBName) + '.sys.dm_exec_cached_plans cp
						ON mg.plan_handle = cp.	plan_handle
					CROSS APPLY sys.dm_exec_sql_text(cp.plan_handle) pt'
	EXEC sp_executesql @SQL
   SET @LoopCounter  = @LoopCounter  + 1        
END

DROP TABLE #DBNames
END


/****** Object:  StoredProcedure [sch_memory].[VLFInfo]    Script Date: 29/05/2019 14:46:49 ******/
SET ANSI_NULLS ON