﻿

-- =============================================
-- Author:		Shaun Salter-Baines
-- Create date: 20.08.2018
-- Description:	Gets the plan handle for any misbehaving queries
-- NOTE  - Needs multiple DB functionality
-- =============================================
CREATE PROCEDURE [sch_memory].[QueryPlanHandle] 
@SPName VARCHAR(100), @DBName VARCHAR(100)


AS


DECLARE
@SQL NVARCHAR(MAX),
@SQL2 NVARCHAR(MAX)

SET @SPName = '''%' + @SPName + '%'''


SET NOCOUNT ON;

IF OBJECT_ID('tempdb..#spStats') IS NOT NULL DROP TABLE #spStats

CREATE TABLE #spStats (
plan_handle VARBINARY(MAX),
creation_time DATETIME,
last_execution_time DATETIME,
execution_count INT,
QueryText VARCHAR(MAX)
)


SET @SQL = '
INSERT INTO #spStats (plan_handle, creation_time, last_execution_time, execution_count, QueryText)
SELECT plan_handle, creation_time, last_execution_time, execution_count, qt.text
FROM 
   ' + QUOTENAME(@DBName)  + '.sys.dm_exec_query_stats qs
   CROSS APPLY ' + QUOTENAME(@DBName)  + '.sys.dm_exec_sql_text (qs.[sql_handle]) AS qt
WHERE qt.text LIKE ' + @SPName
EXEC sp_executesql @SQL


SELECT * FROM #spStats

SELECT 'DBCC FREEPROCCACHE (' + CONVERT(VARCHAR(MAX), plan_handle, 2) + ')' FROM #spStats WHERE execution_count > 1