﻿-- =============================================
-- Author:		Kathryn Mintram
-- Create date: 06/12/2018
-- Description:	Retrieves information regarding VLFs in the transaction log and stores it for monitoring puposes
-- https://blog.sqlauthority.com/2018/02/18/get-vlf-count-size-sql-server-interview-question-week-161/
-- =============================================
CREATE PROCEDURE [sch_memory].[VLFInfo] 
AS
	BEGIN
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		INSERT INTO [sch_memory].[VLFDetails]
		(DatabaseName, DatabaseID, VLFCount, VLFSizeMB, ActiveVLF,
			ActiveVLFSizeMB, InactiveVLF, InactiveVLFSizeMB)
			SELECT [name] DatabaseName, 
				s.database_id DatabaseID,
				COUNT(l.database_id) AS VLFCount,
				SUM(vlf_size_mb) AS VLFSizeMB,
				SUM(CAST(vlf_active AS INT)) AS ActiveVLF,
				SUM(vlf_active*vlf_size_mb) AS ActiveVLFSizeMB,
				COUNT(l.database_id)-SUM(CAST(vlf_active AS INT)) AS InactiveVLF,
				SUM(vlf_size_mb)-SUM(vlf_active*vlf_size_mb) AS InactiveVLFSizeMB
				FROM sys.databases s
			CROSS APPLY sys.dm_db_log_info(s.database_id) l
			WHERE name NOT IN ('master', 'model', 'msdb', 'tempDB')
			GROUP BY [name], s.database_id
			ORDER BY VLFCount DESC
	END


/****** Object:  StoredProcedure [sch_misc].[BaselineStats]    Script Date: 29/05/2019 14:47:23 ******/
SET ANSI_NULLS ON