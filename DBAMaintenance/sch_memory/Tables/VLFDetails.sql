﻿CREATE TABLE [sch_memory].[VLFDetails] (
    [id]                INT             IDENTITY (1, 1) NOT NULL,
    [DatabaseName]      [sysname]       NOT NULL,
    [DatabaseID]        INT             NOT NULL,
    [VLFCount]          INT             NOT NULL,
    [VLFSizeMB]         DECIMAL (18, 5) NOT NULL,
    [ActiveVLF]         INT             NOT NULL,
    [ActiveVLFSizeMB]   DECIMAL (18, 5) NOT NULL,
    [InactiveVLF]       INT             NOT NULL,
    [InactiveVLFSizeMB] DECIMAL (18, 5) NOT NULL,
    [LogDate]           DATETIME        DEFAULT (getdate()) NOT NULL
);

