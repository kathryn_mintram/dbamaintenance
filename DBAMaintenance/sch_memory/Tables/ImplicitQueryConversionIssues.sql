﻿CREATE TABLE [sch_memory].[ImplicitQueryConversionIssues] (
    [Database Name]     NVARCHAR (128) NULL,
    [Query Text]        NVARCHAR (MAX) NULL,
    [Total Worker Time] BIGINT         NULL,
    [Avg Worker Time]   BIGINT         NULL,
    [Max Worker Time]   BIGINT         NULL,
    [Avg Elapsed Time]  BIGINT         NULL,
    [Max Elapsed Time]  BIGINT         NULL,
    [Avg Logical Reads] BIGINT         NULL,
    [Max Logical Reads] BIGINT         NULL,
    [Execution Count]   BIGINT         NULL,
    [Creation Time]     DATETIME       NULL,
    [Query Plan]        XML            NULL
);

