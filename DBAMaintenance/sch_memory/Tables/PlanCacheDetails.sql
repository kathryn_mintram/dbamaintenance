﻿CREATE TABLE [sch_memory].[PlanCacheDetails] (
    [id]           INT            IDENTITY (1, 1) NOT NULL,
    [UseCounts]    INT            NOT NULL,
    [RefCounts]    INT            NOT NULL,
    [CacheObjType] NVARCHAR (128) NOT NULL,
    [ObjType]      NVARCHAR (128) NOT NULL,
    [SizeInBytes]  INT            NOT NULL,
    [DatabaseName] NVARCHAR (50)  NOT NULL,
    [CreatedDate]  DATETIME       NOT NULL,
    [LastExecuted] DATETIME       NOT NULL,
    [Query]        NVARCHAR (MAX) NOT NULL,
    [LogDate]      DATETIME       DEFAULT (getdate()) NOT NULL
);

