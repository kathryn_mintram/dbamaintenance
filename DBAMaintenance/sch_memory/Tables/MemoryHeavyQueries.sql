﻿CREATE TABLE [sch_memory].[MemoryHeavyQueries] (
    [DBName]              NVARCHAR (100) NOT NULL,
    [Request_Time]        DATETIME       NOT NULL,
    [Grant_Time]          DATETIME       NOT NULL,
    [Requested_Memory_KB] INT            NOT NULL,
    [Granted_Memory_KB]   INT            NOT NULL,
    [Required_Memory_KB]  INT            NOT NULL,
    [Used_Memory_KB]      INT            NOT NULL,
    [Max_Used_Memory_KB]  INT            NOT NULL,
    [Wait_Time_MS]        INT            NULL,
    [Query_Text]          NVARCHAR (MAX) NOT NULL,
    [LogDate]             DATETIME       DEFAULT (getdate()) NOT NULL
);

