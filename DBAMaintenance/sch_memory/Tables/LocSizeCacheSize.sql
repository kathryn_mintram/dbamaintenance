﻿CREATE TABLE [sch_memory].[LocSizeCacheSize] (
    [DBFileLocSizeID] INT             IDENTITY (1, 1) NOT NULL,
    [LogicalName]     VARCHAR (50)    NOT NULL,
    [DBName]          VARCHAR (100)   NOT NULL,
    [FileType]        VARCHAR (50)    NOT NULL,
    [FileLoc]         VARCHAR (250)   NOT NULL,
    [State_Desc]      VARCHAR (50)    NOT NULL,
    [DateFileSizeGB]  NUMERIC (10, 2) NOT NULL,
    [CachedDataMB]    NUMERIC (10, 2) NOT NULL,
    CONSTRAINT [PK_DBFileLocSizeID] PRIMARY KEY CLUSTERED ([DBFileLocSizeID] ASC)
);

