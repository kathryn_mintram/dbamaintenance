﻿CREATE TABLE [sch_memory].[BufferCacheDetails] (
    [id]                 INT             IDENTITY (1, 1) NOT NULL,
    [IndexName]          NVARCHAR (128)  NULL,
    [ObjectName]         NVARCHAR (128)  NOT NULL,
    [ObjectType]         NVARCHAR (128)  NOT NULL,
    [BufferPages]        INT             NOT NULL,
    [BufferTotalMB]      DECIMAL (18, 2) NOT NULL,
    [FreeSpaceMB]        DECIMAL (18, 2) NOT NULL,
    [BufferFreeSpacePct] DECIMAL (5, 2)  NOT NULL,
    [DirtyPages]         INT             NOT NULL,
    [CleanPages]         INT             NOT NULL,
    [DirtyPagesMB]       DECIMAL (18, 2) NOT NULL,
    [CleanPagesMB]       DECIMAL (18, 2) NOT NULL,
    [LogDate]            DATETIME        DEFAULT (getdate()) NOT NULL
);

