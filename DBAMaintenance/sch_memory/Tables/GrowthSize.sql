﻿CREATE TABLE [sch_memory].[GrowthSize] (
    [DBFileGrowthID] INT             IDENTITY (1, 1) NOT NULL,
    [StartTime]      DATETIME        NOT NULL,
    [EventName]      VARCHAR (100)   NOT NULL,
    [DatabaseName]   VARCHAR (50)    NOT NULL,
    [FileName]       VARCHAR (50)    NOT NULL,
    [GrowthMB]       NUMERIC (10, 2) NOT NULL,
    [DataFileSizeGB] NUMERIC (10, 2) NOT NULL,
    CONSTRAINT [PK_DBFileGrowthID] PRIMARY KEY CLUSTERED ([DBFileGrowthID] ASC)
);

